package com.gdicristofaro.ast.test;

import java.lang.reflect.Field;

import com.gdicristofaro.ast.Expression;
import com.gdicristofaro.ast.GrammarParser;
import com.gdicristofaro.ast.ParseResult;
import com.gdicristofaro.ast.RecursiveDescentParser;

public class ASTPrintExample {
	private static String NEW_LINE = System.lineSeparator();
	
	public static final String NUMBER_GRAMMAR_STR = 
		"<numberexpression> ::= <multexpr> <wso> <addop> <wso> <numberexpression> | <multexpr>" + NEW_LINE +
		"<addop> ::= '\\+' | '-'" + NEW_LINE +
		"<multexpr> ::= <negatedexpr> <wso> <multop> <wso> <multexpr> | <negatedexpr>" + NEW_LINE +
		"<multop> ::= '\\*' | '\\/'" + NEW_LINE +
		"<negatedexpr> ::= '-' <exponentexpr> | <exponentexpr>" + NEW_LINE +
		"<exponentexpr> ::= <basenumber> <wso> '\\^' <wso> <basenumber> | <basenumber>" + NEW_LINE +
		"<basenumber> ::= <number> | '\\('  <wso> <numberexpression>  <wso> '\\)'" + NEW_LINE +
		"<number> ::=  '[-]?\\d+'" + NEW_LINE +
		"<wso> ::= '[ \\\\t]*'" + NEW_LINE;
	
	private static String NUMBER_TEST_STR = "( 1 + 3 ) * 3 + -3 ^ 5";
	
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		// access the parser that parses a grammar to an AST
		Field f = GrammarParser.class.getDeclaredField("Parser");
		f.setAccessible(true);
		RecursiveDescentParser grammarParser = (RecursiveDescentParser) f.get(null);
				
		ParseResult result = grammarParser.parse(NUMBER_GRAMMAR_STR);
		
		System.out.println("-----AST for math expressions grammar------");
		System.out.println((result.getParsed().getTreeString(true)));
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		Expression leadMathExp = GrammarParser.parseGrammar(NUMBER_GRAMMAR_STR);
		RecursiveDescentParser numParser = new RecursiveDescentParser(leadMathExp);
		System.out.println("-----AST for expression: \"" + NUMBER_TEST_STR + "\" -----");
		System.out.println((numParser.parse(NUMBER_TEST_STR).getParsed().getTreeString(true)));
	}
}
