package com.gdicristofaro.ast.test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import com.gdicristofaro.ast.TokenLiteral;
import com.gdicristofaro.ast.Tuple;
import com.gdicristofaro.ast.Utils;
import com.gdicristofaro.ast.ASTComponent;
import com.gdicristofaro.ast.ASTLiteral;
import com.gdicristofaro.ast.ASTNode;
import com.gdicristofaro.ast.Expression;
import com.gdicristofaro.ast.GrammarParser;
import com.gdicristofaro.ast.ParseResult;
import com.gdicristofaro.ast.RecursiveDescentParser;
import com.gdicristofaro.ast.TokenExpression;

class ASTTests {
	private static String NEW_LINE = System.lineSeparator();

	// simple number grammar for testing ast tree recursive descent parser
	public static final String NUMBER_GRAMMAR_STR =
		"<numberexpression> ::= <multexpr> <wso> <addop> <wso> <numberexpression> | <multexpr>" + NEW_LINE +
		"<addop> ::= '\\+' | '-'" + NEW_LINE +
		"<multexpr> ::= <negatedexpr> <wso> <multop> <wso> <multexpr> | <negatedexpr>" + NEW_LINE +
		"<multop> ::= '\\*' | '\\/'" + NEW_LINE +
		"<negatedexpr> ::= '-' <exponentexpr> | <exponentexpr>" + NEW_LINE +
		"<exponentexpr> ::= <basenumber> <wso> '\\^' <wso> <basenumber> | <basenumber>" + NEW_LINE +
		"<basenumber> ::= <number> | '\\('  <wso> <numberexpression>  <wso> '\\)'" + NEW_LINE +
		"<number> ::=  '[-]?\\d+'" + NEW_LINE +
		"<wso> ::= '[ \\\\t]*'" + NEW_LINE;

	@Test
	void tokenLiteralTest() {
		String testString = "this is a test string";
		// empty string regex (in different positions)
		TokenLiteral empty = new TokenLiteral("");

		ParseResult res = empty.handle(testString, 0, null);
		assertTrue(res.isSuccess());

		ParseResult res2 = empty.handle(testString, 10, null);
		assertTrue(res2.isSuccess());

		ParseResult res3 = empty.handle(testString, -10, null);
		assertTrue(!res3.isSuccess());

		ParseResult res4 = empty.handle(testString, 100, null);
		assertTrue(!res4.isSuccess());
	}

	@Test
	void tokenLiteralTest2() {
		String testString = "this is a test string";
		// empty string regex (in different positions)
		TokenLiteral empty = new TokenLiteral("\\s");

		ParseResult res = empty.handle(testString, 4, null);
		assertTrue(res.isSuccess());

		ParseResult res2 = empty.handle(testString, 7, null);
		assertTrue(res2.isSuccess());

		ParseResult res3 = empty.handle(testString, 0, null);
		assertTrue(!res3.isSuccess());

		ParseResult res4 = empty.handle(testString, 3, null);
		assertTrue(!res4.isSuccess());

		ParseResult res5 = empty.handle(testString, -10, null);
		assertTrue(!res5.isSuccess());

		ParseResult res6 = empty.handle(testString, 100, null);
		assertTrue(!res6.isSuccess());
	}

	@Test
	void indexToLineColTest() {
		String text = "line 1\nnext line\nline after that\none more line";
		assertEquals(Utils.IndexToLineCol(text, 0), new Tuple<Integer,Integer>(1,1));
		assertEquals(Utils.IndexToLineCol(text, 9), new Tuple<Integer,Integer>(2,3));
		assertEquals(Utils.IndexToLineCol(text, 45), new Tuple<Integer,Integer>(4,13));
		assertEquals(Utils.IndexToLineCol(text, -2), null);
		assertEquals(Utils.IndexToLineCol(text, 46), null);
	}

	@Test
	void getTreeStringTest() {
		ASTLiteral a1 = new ASTLiteral(0, "a1");
		ASTLiteral a2 = new ASTLiteral(0, "a2");
		ASTNode a = new ASTNode(0, new Expression(null, "a"), 0, new ASTComponent[] {a1, a2});
		ASTLiteral b = new ASTLiteral(0,"b");
		ASTLiteral c = new ASTLiteral(0,"c");
		ASTNode tree = new ASTNode(0,new Expression(null, "tree"), 0, new ASTComponent[] {a, b, c});

		String str = tree.getTreeString(false);

		String[] lines = str.split("\n");
		assertTrue(lines[0].contains("tree"));

		assertTrue(lines[1].contains("a"));
		assertTrue(lines[1].indexOf("a") > lines[0].indexOf("tree"));

		assertTrue(lines[2].contains("a1"));
		assertTrue(lines[2].indexOf("a1") > lines[1].indexOf("a"));

		assertTrue(lines[3].contains("a2"));
		assertTrue(lines[3].indexOf("a2") > lines[1].indexOf("a"));

		assertTrue(lines[4].contains("b"));
		assertTrue(lines[4].indexOf("b") > lines[0].indexOf("tree"));

		assertTrue(lines[5].contains("c"));
		assertTrue(lines[5].indexOf("c") > lines[0].indexOf("tree"));
	}

	@Test
	void parseGrammarTest() {
		Expression rootExp = GrammarParser.parseGrammar(NUMBER_GRAMMAR_STR);
		assertEquals(rootExp.getType(), "numberexpression");
		assertEquals(rootExp.getChoices().length, 2);
		assertEquals(rootExp.getChoices()[0].getTokens().length, 5);

		//satisfies <multexpr> <wso> <addop> <wso> <numberexpression>
		assertEquals(((TokenExpression)rootExp.getChoices()[0].getTokens()[0]).getExpression().getType(), "multexpr");
		assertEquals(((TokenExpression)rootExp.getChoices()[0].getTokens()[1]).getExpression().getType(), "wso");
		assertEquals(((TokenExpression)rootExp.getChoices()[0].getTokens()[2]).getExpression().getType(), "addop");
		assertEquals(((TokenExpression)rootExp.getChoices()[0].getTokens()[3]).getExpression().getType(), "wso");
		assertEquals(((TokenExpression)rootExp.getChoices()[0].getTokens()[4]).getExpression().getType(), "numberexpression");
	}

	@Test
	void recursiveDescentParserTest() {
		ParseResult res1 = new RecursiveDescentParser(GrammarParser.parseGrammar(NUMBER_GRAMMAR_STR)).parse("hello world");
		assertTrue(!res1.isSuccess());

		ParseResult res2 = new RecursiveDescentParser(GrammarParser.parseGrammar(NUMBER_GRAMMAR_STR)).parse("5 + 3");
		assertTrue(res2.isSuccess());

	}
}
