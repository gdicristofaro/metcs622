package com.gdicristofaro.symboltable.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import com.gdicristofaro.ast.ASTNode;
import com.gdicristofaro.ast.Expression;
import com.gdicristofaro.symboltable.DefaultSymbols.SymbolPayload;
import com.gdicristofaro.symboltable.SymbolTable;

public class SymbolTableTests {
	@Test
	public void addScopeTest() {
		SymbolTable<SymbolPayload> table = new SymbolTable<>(null, new ASTNode(0, new Expression(null, "type1"), 0, null));
		table.addScope(new ASTNode(0, new Expression(null, "type2"), 0, null));

		assertEquals(table.getChildren().size(), 1);
		assertEquals(table.getChildren().get(0).getNode().getExpressionType().getType(), "type2");
	}
	
	
	@Test
	public void addSymbolTest() {
		SymbolTable<SymbolPayload> table = new SymbolTable<>(null, new ASTNode(0, new Expression(null, "type1"), 0, null));
		table.addSymbol("var1", new SymbolPayload("int", false));
		
		assertNotEquals(table.getSymbolData("var1"), null);
		assertEquals(table.getSymbolData("var1").getType(), "int");
	}
	
	
	@Test
	public void containsSymbolTest() {
		SymbolTable<SymbolPayload> table = new SymbolTable<>(null, new ASTNode(0, new Expression(null, "type1"), 0, null));
		table.addSymbol("varTable", new SymbolPayload("string", true));
		
		SymbolTable<SymbolPayload> child = table.addScope(new ASTNode(0, new Expression(null, "type2"), 0, null));
		child.addSymbol("varChild", new SymbolPayload("string", true));
		
		assertTrue(child.containsSymbol("varChild", true));
		assertTrue(child.containsSymbol("varChild", true));
		
		assertFalse(child.containsSymbol("varTable", true));
		assertTrue(child.containsSymbol("varTable", false));
		
		assertNotEquals(child.getSymbolData("varChild"), null);
		assertNotEquals(child.getSymbolData("varTable"), null);
		assertEquals(child.getSymbolData("varTBD"), null);
	}
}
