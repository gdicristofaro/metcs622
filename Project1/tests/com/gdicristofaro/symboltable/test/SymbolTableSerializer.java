package com.gdicristofaro.symboltable.test;

import java.io.IOException;

import com.gdicristofaro.symboltable.DefaultSymbols;
import com.gdicristofaro.symboltable.DefaultSymbols.SymbolPayload;
import com.gdicristofaro.symboltable.Symbol;
import com.gdicristofaro.symboltable.SymbolTableSerialization;
import com.gdicristofaro.symboltable.SymbolTableSerialization.SerializationDTO;

public class SymbolTableSerializer {
	/**
	 * simply serializes the default serialization items to the path provided
	 * @param args		possibly the serialization path
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		if (args == null || args.length == 0)
			args = new String[] {"symboltable.dat"};
			
		@SuppressWarnings("unchecked")
		SerializationDTO<SymbolPayload> dto = new SerializationDTO<>(
			(Symbol<SymbolPayload>[]) new Symbol[]{ DefaultSymbols.VARIABLE_DECLARATION }, 
			DefaultSymbols.DEFAULT_SCOPE_PARENTS,
			DefaultSymbols.DEFAULT_CONVERTER);

		SymbolTableSerialization.serialize(dto, args[0]);
	}
}
