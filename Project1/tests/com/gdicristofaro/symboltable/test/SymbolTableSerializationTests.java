package com.gdicristofaro.symboltable.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.gdicristofaro.symboltable.DefaultSymbols;
import com.gdicristofaro.symboltable.DefaultSymbols.SymbolPayload;
import com.gdicristofaro.symboltable.Symbol;
import com.gdicristofaro.symboltable.SymbolTableSerialization;
import com.gdicristofaro.symboltable.SymbolTableSerialization.SerializationDTO;

public class SymbolTableSerializationTests {
	
	@Test
	public void serializationTest() throws IOException, ClassNotFoundException {
		@SuppressWarnings("unchecked")
		SerializationDTO<SymbolPayload> dto = new SerializationDTO<>(
			(Symbol<SymbolPayload>[]) new Symbol[]{ DefaultSymbols.VARIABLE_DECLARATION }, 
			DefaultSymbols.DEFAULT_SCOPE_PARENTS,
			(p) -> String.format("{ type: %s, initialized: %b }", p.getType(), p.isInitialized()));
		
		File tempPath = File.createTempFile("symbolSerialization", "dat");
		SymbolTableSerialization.serialize(dto, tempPath.getAbsolutePath());
		
		SerializationDTO<SymbolPayload> reDto = SymbolTableSerialization.deserialize(tempPath.getAbsolutePath());
		
		assertEquals(dto.getScopeItems().length, reDto.getScopeItems().length);
		assertEquals(dto.getSymbols().length, reDto.getSymbols().length);
	}
}
