title Database Usage

User->ToyParser:1) User runs ToyParser with the -l\nflag so that the session is logged.
ToyParser->Derby Database: Data pertaining to the run, AST Nodes and\nvariables is inserted into the database.

User->ToyParser:2) User requests log of runs
ToyParser->Derby Database:ToyParser selects from the Run\ntable in Derby Database
Derby Database->ToyParser:Derby Database returns all runs
ToyParser->User:ToyParser prints all data from the runs

User->ToyParser:3) User requests metrics
ToyParser->Derby Database:ToyParser creates select statements to determine\naverage variable depth, most common variable names\nand most common scope parent
Derby Database->ToyParser:Derby Database returns all pertinent data from\nselect statements
ToyParser->User:ToyParser prints all data about metrics for user