package com.gdicristofaro.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.gdicristofaro.ast.ASTComponent;
import com.gdicristofaro.ast.ASTLiteral;
import com.gdicristofaro.ast.ASTNode;
import com.gdicristofaro.ast.Func;
import com.gdicristofaro.symboltable.SymbolTable;

public class Dao {
	private static Connection _Conn;
	
	/*
	 * The method creates a Connection object. Loads the embedded driver,
	 * starts and connects to the database using the connection URL.
	 */
	public static Connection getDatabaseConnection()
			throws SQLException, ClassNotFoundException {
		if (_Conn != null)
			return _Conn;
		
		String driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Class.forName(driver);
		String url = "jdbc:derby:appDB;create=true";
		Connection c = DriverManager.getConnection(url);
		return c;
	}

	/**
	 * creates a table if doesn't exist (WARNING: not prepared statements as is schema related; make sure strings are sanitized)
	 * @param c				connection
	 * @param schemaName		the schema of the table
	 * @param tableName		the table name
	 * @param tableSchema	what the schema of the table should be
	 */
	protected static void createIfNotExists(Connection c, String schemaName, String tableName, String... tableSchema) {
		try {
			DatabaseMetaData dbmd = c.getMetaData();
			ResultSet tables = dbmd.getTables(null, schemaName.toUpperCase(), tableName.toUpperCase(), null);
			if(!tables.next()) {
				c.createStatement().execute(String.format("CREATE TABLE %s.%s (%s)", 
											schemaName, tableName, String.join(", ", tableSchema)));
			}
		}
		catch (SQLException e) {
			throw new IllegalStateException(String.format("error creating table: [%s].[%s]", schemaName, tableName), e);
		}
	}
	
	/**
	 * creates tables necessary for the application
	 * @param c		the database connection
	 */
	public static void createTables(Connection c) {
		String autoId = "id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)";
		String primaryKey = "PRIMARY KEY (id)";
		
		// creates run table
		createIfNotExists(c, 
			"dbo", "run", 
			autoId,
			"date TIMESTAMP",
			"contentFile VARCHAR(200)",
			"grammarFile VARCHAR(200)",
			"outputFile VARCHAR(200)",
			primaryKey);
		
		// creates ast node table
		createIfNotExists(c, 
			"dbo", "astnode", 
			autoId,
			"nodename VARCHAR(100)",
			"nodetext VARCHAR(200)",
			"parentid INT",
			"runid INT",
			"FOREIGN KEY(parentid) REFERENCES DBO.ASTNODE(id)",
			"FOREIGN KEY(runid) REFERENCES DBO.RUN(id)",
			primaryKey);
		
		// creates variable table
		createIfNotExists(c, 
			"dbo", "variable", 
			autoId,
			"name VARCHAR(100)",
			"depth INT",
			"runid INT",
			"astparentid INT",
			"stringInf VARCHAR(200)",
			"FOREIGN KEY(runid) REFERENCES DBO.RUN(id)",
			"FOREIGN KEY(astparentid) REFERENCES DBO.ASTNODE(id)",
			primaryKey);
	}
	
	/**
	 * inserts data about a run into the database
	 * @param contentFile		the name of the content file
	 * @param grammarFile		the name of the grammar file
	 * @param outputFile			the name of the output file
	 * @param root				the root ast node
	 * @param rootSymbolTable	the root symbol table (if created)
	 * @param payloadConverter	the means of converting the variable payload in a symbol table
	 */
	public static <P> void insertData(String contentFile, String grammarFile, String outputFile, ASTNode root,
									SymbolTable<P> rootSymbolTable, Func<P,String> payloadConverter) {
		
		try {
			Connection c = getDatabaseConnection();
			// create tables if they don't exist
			createTables(c);
			Instant instant = Instant.now();
			int runid = insert(c, "DBO.RUN", 
				new String[] {"date", "contentFile", "grammarFile", "outputFile"}, 
				new Object[] {Timestamp.from(instant), contentFile, grammarFile, outputFile});
			
			Map<ASTComponent, Integer> items = new HashMap<>();
			insertAstNodeRecursive(c, items, runid, null, root);
			
			if (rootSymbolTable != null)
				insertRootSymbolTable(c, items, rootSymbolTable, payloadConverter, runid, 0);	
		}
		catch (SQLException | ClassNotFoundException e) {
			throw new IllegalStateException("unable to connect to database", e);
		}
	}
	
	/**
	 * base insert method into the database
	 * @param c			the database connection
	 * @param tableName	the name of the table to insert into; make sure data is sanitized
	 * @param cols		the name of the columns; make sure data is sanitized
	 * @param values		the values to insert
	 * @return			the id of the row inserted
	 */
	protected static int insert(Connection c, String tableName, String[] cols, Object[] values) {
		try {
			if (values == null || values.length == 0)
				throw new IllegalStateException("There must be at least one value to insert");
			
			String colsStr = "(" + String.join(",",  cols) + ")";
			String valuesStr = "(" + Arrays.stream(values).map(s -> "?").collect(Collectors.joining(",")) + ")";
		    PreparedStatement ps = c.prepareStatement(String.format("INSERT INTO %s %s VALUES %s", tableName, colsStr, valuesStr), new String[] { "ID"});

		    	for (int i = 0; i < values.length; i++)
		    		ps.setObject(i + 1, values[i]);
		    
		    ps.executeUpdate();
		    ResultSet rs = ps.getGeneratedKeys(); 
		    rs.next();
		    return rs.getInt(1);
		}
		catch (SQLException e) {
			throw new IllegalStateException(String.format("There was an error inserting into %s with arguments %s", 
				tableName, Arrays.stream(values).map(s -> s.toString()).collect(Collectors.joining(","))), e);
		}
	}
	
	/**
	 * inserts data into the database about a symbol table
	 * @param c					the database connection
	 * @param mapping			the mapping of ast components to their row id's 
	 * @param table				the symbol table to add to the db
	 * @param payloadConverter	converts the symbol table variable payload to a string
	 * @param runid				the id of the run
	 * @param depth				the current depth of the symbol table
	 */
	private static <P> void insertRootSymbolTable(Connection c, Map<ASTComponent, Integer> mapping, SymbolTable<P> table, 
												Func<P, String> payloadConverter, int runid, int depth) {
		
		int parentId = mapping.get(table.getNode());
		for (Entry<String, P> ent : table.getAllSymbolData().entrySet()) {
			insert(c, "DBO.VARIABLE", 
				new String[] {"name", "depth", "runid", "astparentid", "stringInf"}, 
				new Object[] {ent.getKey(), depth, runid, parentId, payloadConverter.convert(ent.getValue())});
		}
		
		for (SymbolTable<P> childTable : table.getChildren())
			insertRootSymbolTable(c, mapping, childTable, payloadConverter, runid, depth + 1);
	}
	
	/**
	 * recursively inserts ast nodes into the database
	 * @param c			the database connection
	 * @param mapping	the mapping of ast components to row id (created as items added)
	 * @param runid		the id of the run
	 * @param parentId	the id of the parent if the item has a parent
	 * @param comp		the component to insert
	 */
	private static void insertAstNodeRecursive(Connection c, Map<ASTComponent, Integer> mapping, 
											int runid, Integer parentId, ASTComponent comp) {
		if (comp == null)
			return;
		
		if (comp instanceof ASTNode) {
			int id = insert(c, "DBO.ASTNODE", new String[] {"nodename", "parentid", "runid"},
				 new Object[] {((ASTNode) comp).getExpressionType().getType(), parentId, runid});
			
			mapping.put(comp, id);
			
			if (comp.getComponents() != null) {
				for (ASTComponent child : comp.getComponents()) {
					insertAstNodeRecursive(c, mapping, runid, id, child);
				}
			}
					
		}
		else if (comp instanceof ASTLiteral) {
			int id = insert(c, "DBO.ASTNODE", new String[] {"nodetext", "parentid", "runid"},
				 new Object[] {((ASTLiteral) comp).getValue(), parentId, runid});	
			
			mapping.put(comp, id);
		}
	}
	
	/**
	 * obtains the most common variable names in the database
	 * @return		the most common variable names in the database
	 */
	public static List<String> mostCommonVariables() {
		try {
			Connection c = getDatabaseConnection();
			// create tables if they don't exist
			createTables(c);
			ResultSet rs = c.createStatement().executeQuery("SELECT name FROM DBO.VARIABLE GROUP BY name ORDER BY COUNT(*) DESC FETCH FIRST 10 ROWS ONLY");
			List<String> strs = new ArrayList<>();
			
			while (rs.next())
				strs.add(rs.getString(1));
			
			return strs;
		}
		catch (SQLException | ClassNotFoundException e) {
			throw new IllegalStateException("There was an error retrieving most common variable names", e);
		}
	}
	
	/**
	 * returns the most common scope parent items
	 * @return		a list of the most common scope parents
	 */
	public static List<String> mostCommonScopeParent() {
		try {
			Connection c = getDatabaseConnection();
			// create tables if they don't exist
			createTables(c);
			ResultSet rs = c.createStatement().executeQuery(
				"SELECT a.nodename FROM DBO.VARIABLE AS v " + 
				"INNER JOIN DBO.ASTNODE AS a " + 
					"ON v.astparentid = a.id " + 
					"GROUP BY a.nodename " + 
					"ORDER BY COUNT(*) DESC " + 
					"FETCH FIRST 10 ROWS ONLY");
			
			List<String> strs = new ArrayList<>();
			while (rs.next())
				strs.add(rs.getString(1));
			
			return strs;
		}
		catch (SQLException | ClassNotFoundException e) {
			throw new IllegalStateException("There was an error retrieving most common scope parents", e);
		}
	}
	
	/**
	 * 
	 * @return		the most common scope depth of variables (i.e. while (...) { if (...) { variable declaration } } would be 2
	 */
	public static Double averageScopeDepth() {
		try {
			Connection c = getDatabaseConnection();
			// create tables if they don't exist
			createTables(c);
			ResultSet rs = c.createStatement().executeQuery("SELECT AVG(CAST(depth AS DOUBLE)) FROM DBO.VARIABLE");
			if (rs.next())
				return rs.getDouble(1);
			else
				return null;
		}
		catch (SQLException | ClassNotFoundException e) {
			throw new IllegalStateException("There was an error retrieving average scope depth", e);
		}
	}
	
	/**
	 * information pertaining to a run of the program
	 */
	public static class RunInfo {
		private final String contentFile;
		private final String grammarFile;
		private final String outputFile;
		private final Date time;
		private final int astnodes;
		private final int symbols;
		
		/**
		 * main constructor
		 * @param contentFile		the content file name
		 * @param grammarFile		the grammar file name
		 * @param outputFile			the output file name
		 * @param time				the time of the run
		 * @param astnodes			the number of ast nodes
		 * @param symbols			the number of symbols
		 */
		public RunInfo(String contentFile, String grammarFile, String outputFile, Date time, int astnodes,
				int symbols) {
			this.contentFile = contentFile;
			this.grammarFile = grammarFile;
			this.outputFile = outputFile;
			this.time = time;
			this.astnodes = astnodes;
			this.symbols = symbols;
		}

		/**
		 * @return		the content file name
		 */
		public String getContentFile() {
			return contentFile;
		}

		/**
		 * 
		 * @return		the grammar file name
		 */
		public String getGrammarFile() {
			return grammarFile;
		}

		/**
		 * 
		 * @return		the output file name
		 */
		public String getOutputFile() {
			return outputFile;
		}

		/**
		 * 
		 * @return		the date of the run
		 */
		public Date getTime() {
			return time;
		}

		/**
		 * 
		 * @return		the number of ast nodes for the run
		 */
		public int getAstnodes() {
			return astnodes;
		}

		/**
		 * 
		 * @return		the number of symbols for the run
		 */
		public int getSymbols() {
			return symbols;
		}
	}

	/**
	 * 
	 * @return		returns all runs in the database
	 */
	public static List<RunInfo> getRuns() {
		try {
			Connection c = getDatabaseConnection();
			// create tables if they don't exist
			createTables(c);
			ResultSet rs = c.createStatement().executeQuery(
				"SELECT r.contentFile, r.grammarFile, r.outputFile, r.date, " +
					"(SELECT COUNT(*) FROM DBO.ASTNODE AS a WHERE a.runid = r.id) AS nodes, " + 
					"(SELECT COUNT(*) FROM DBO.VARIABLE AS V WHERE v.runid = r.id) AS variables " + 
					"FROM DBO.RUN AS r " + 
					"ORDER BY r.date DESC");
			
			List<RunInfo> runs = new ArrayList<>();
			while (rs.next())
				runs.add(new RunInfo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getInt(5), rs.getInt(6)));
			
			return runs;
		}
		catch (SQLException | ClassNotFoundException e) {
			throw new IllegalStateException("There was an error retrieving most common most common scope parents", e);
		}
	}
}
