package com.gdicristofaro.symboltable;

import com.gdicristofaro.ast.Func;
import com.gdicristofaro.ast.Tuple;

/**
 * defines some default symbols and scope parents for symbol table construction
 */
public class DefaultSymbols {
	/**
	 * the default payload for a symbol (contains a type field and whether or not the symbol has been initialized)
	 */
	public static class SymbolPayload {
		private final String type;
		private final boolean isInitialized;

		public SymbolPayload(String type, boolean isInitialized) {
			this.type = type;
			this.isInitialized = isInitialized;
		}

		public String getType() {
			return type;
		}
		
		public boolean isInitialized() {
			return isInitialized;
		}
	}
	
	/**
	 * converts a symbol payload to a string
	 */
	public static final Func<SymbolPayload, String> DEFAULT_CONVERTER = (p) -> String.format("{ type: %s, initialized: %b }", p.getType(), p.isInitialized());

	/**
	 * this rule is for capturing variable declarations that assign values
	 */
	public static Symbol<SymbolPayload> VARIABLE_DECLARATION = new Symbol<SymbolPayload>("variabledeclaration", (nd) ->
		new Tuple<String, SymbolPayload>(nd.getChild("variable").extractVal(), new SymbolPayload(nd.getChild("type").extractVal(), nd.getChild("assignmentexpression") != null)));
	
	/**
	 * a while loop ast node scope parent
	 */
	public final static ScopeParent WHILE_PARENT = new ScopeParent("whilestatement", (nd) -> nd);
	
	/**
	 * an 'if' ast node scope parent
	 */
	public final static ScopeParent IF_PARENT = new ScopeParent("ifstatement", (nd) -> nd);
	
	/**
	 * an 'else if' ast node scope parent
	 */
	public final static ScopeParent ELSE_IF_PARENT = new ScopeParent("elseifstatement", (nd) -> nd);
	
	/**
	 * an 'else' ast node scope parent
	 */
	public final static ScopeParent ELSE_PARENT = new ScopeParent("elsestatement", (nd) -> nd);
	
	/**
	 * a 'for' ast node scope parent
	 */
	public final static ScopeParent FOR_STATEMENT = new ScopeParent("forstatement", (nd) -> nd);
	
	/**
	 * the default scope parents
	 */
	public final static ScopeParent[] DEFAULT_SCOPE_PARENTS = new ScopeParent[] { WHILE_PARENT, IF_PARENT, ELSE_IF_PARENT, ELSE_PARENT, FOR_STATEMENT }; 
}
