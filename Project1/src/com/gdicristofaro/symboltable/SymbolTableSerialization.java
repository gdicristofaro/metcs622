package com.gdicristofaro.symboltable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.gdicristofaro.ast.Func;

public class SymbolTableSerialization {
	/**
	 * the object representing symbol table parsing elements to serialize and deserialize
	 */
	public static class SerializationDTO<P> implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6217536582723451977L;
		
		private final Symbol<P>[] symbols;
		private final ScopeParent[] scopeItems;
		private final Func<P,String> payloadStringify;
		
		/**
		 * main constructor for serialization dto object
		 * @param symbols		the symbols representing variable declarations
		 * @param scopeItems		the items triggering a new scope
		 */
		public SerializationDTO(Symbol<P>[] symbols, ScopeParent[] scopeItems, Func<P,String> payloadStringify) {
			this.symbols = symbols;
			this.scopeItems = scopeItems;
			this.payloadStringify = payloadStringify;
		}

		/**
		 * @return		the symbols representing a variable declaration
		 */
		public Symbol<P>[] getSymbols() {
			return symbols;
		}

		/**
		 * @return		the items that trigger a change in scope
		 */
		public ScopeParent[] getScopeItems() {
			return scopeItems;
		}

		/**
		 * @return		generates a string from a payload specified
		 */
		public Func<P, String> getPayloadStringify() {
			return payloadStringify;
		}
		
		
	}
	
	
	/**
	 * serializes a symbol table serialization dto object
	 * @param serializations		the object to be serialized
	 * @param path				the path for where the item will be serialized
	 * @throws IOException		throws an IOException due to file access issues
	 */
	public static <P> void serialize(SerializationDTO<P> serializations, String path) throws IOException {
        FileOutputStream file = null;
        ObjectOutputStream out = null;
		try {
	        file = new FileOutputStream(path);
	        out = new ObjectOutputStream(file);
	         
	        out.writeObject(serializations);
		}
		finally {
			if (out != null)
				out.close();
			
			if (file != null)
				file.close();			
		}
	}
	
	/**
	 * deserializes a symbol table serialization dto object
	 * @param path						the path where the serialization object exists
	 * @return							the deserialized object
	 * @throws IOException				throws an IOException due to file access issues
	 * @throws ClassNotFoundException	if the cast to the appropriate object is unsuccessful.
	 */
	@SuppressWarnings("unchecked")
	public static <P> SerializationDTO<P> deserialize(String path) throws IOException, ClassNotFoundException {
		FileInputStream file = null;
        ObjectInputStream in = null;
        Object toRet = null;
		try {
	        file = new FileInputStream(path);
	        in = new ObjectInputStream(file);
	         
	        toRet = in.readObject();
		}
		catch (Exception e) {
			System.out.println("Error processing file: " + e.getMessage());
		}
		finally {
			if (in != null)
				in.close();
			
			if (file != null)
				file.close();			
		}
		
		return (SerializationDTO<P>)toRet;
	}
	
}
