package com.gdicristofaro.symboltable;

import java.io.Serializable;

import com.gdicristofaro.ast.ASTNode;
import com.gdicristofaro.ast.Func;
import com.gdicristofaro.ast.Tuple;

/**
 * represents a means to make a symbol in a symbol table
 * 
 * @param <P>		the symbol payload
 */
public class Symbol<P> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6900141377230721285L;
	
	private final String expressionType;
	private final Func<ASTNode, Tuple<String, P>> payloadExtract;
	
	/**
	 * the main constructor for a symbol
	 * @param expressionType		the expression type as specified by the bnf expression (i.e. 'variabledeclaration' for <variabledeclaration> ::= ...)
	 * @param payloadExtract		a function to generate the variable name and payload based on the item
	 */
	public Symbol(String expressionType, Func<ASTNode, Tuple<String, P>> payloadExtract) {
		this.expressionType = expressionType;
		this.payloadExtract = payloadExtract;
	}

	/**
	 * @return the expression type for this symbol 
	 */
	public String getExpressionType() {
		return expressionType;
	}

	/**
	 * given an appropriate ASTNode, extracts the payload
	 * @param child		the child for which to extract payload
	 * @return			the variable name / payload
	 */
	public Tuple<String, P> extractPayload(ASTNode child) {
		if (!child.getExpressionType().getType().equals(expressionType))
			throw new IllegalStateException(String.format("Node is of type %s and not of expected type %s.", child.getExpressionType().getType(), expressionType));
		
		return payloadExtract.convert(child);
	}
}
