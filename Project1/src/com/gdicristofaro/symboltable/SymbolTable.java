package com.gdicristofaro.symboltable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.gdicristofaro.ast.ASTComponent;
import com.gdicristofaro.ast.ASTNode;
import com.gdicristofaro.ast.Func;
import com.gdicristofaro.ast.Tuple;
import com.gdicristofaro.ast.Utils;

/**
 * represents a symbol table within a specific scope
 * @param <P>	represents the payload type for a symbol 
 */
public class SymbolTable<P> {
	
	/**
	 * represents a node belonging to this symbol table (a variable or child symbol table)
	 * @param <P>
	 */
	public static class SymbolTableNode<P> {
		private final SymbolTable<P> symTable;
		private final String symbol;
		private final boolean isSymbol;
		private final P symbolData;
		
		/**
		 * generates a child node for a symbol table
		 * @param symTable		the symbol table child for the item
		 */
		public SymbolTableNode(SymbolTable<P> symTable) {
			this.symTable = symTable;
			this.isSymbol = false;
			this.symbol = null;
			this.symbolData = null;
		}

		/**
		 * generates a child node for a variable
		 * @param symbol			the variable name
		 * @param symbolData		the data for the symbol
		 */
		public SymbolTableNode(String symbol, P symbolData) {
			this.symTable = null;
			this.isSymbol = true;
			this.symbol = symbol;
			this.symbolData = symbolData;
		}
		
		public SymbolTable<P> getSymTable() { return symTable; }
		public String getSymbol() { return symbol; }
		public boolean isSymbol() { return isSymbol; }
		public P getSymbolData() { return symbolData; }
	}
	
	private final ASTNode node;
	private final Map<String, P> scopedSymbols = new HashMap<>();
	private final List<SymbolTable<P>> childTables = new ArrayList<>();
	private final List<SymbolTableNode<P>> items = new ArrayList<SymbolTableNode<P>>();
	private final SymbolTable<P> parent;
	
	/**
	 * main constructor for a symbol table
	 * @param parent			the parent of this symbol table (or null if does not exist)
	 * @param node			the node applicable for this symbol table
	 */
	public SymbolTable(SymbolTable<P> parent, ASTNode node) {
		this.parent = parent;
		this.node = node;
	}
	
	/**
	 * adds a new scope (and consequent symbol table) for this symbol table
	 * @param node	the node for which to add this new scope
	 * @return		the child symbol table
	 */
	public SymbolTable<P> addScope(ASTNode node) {
		SymbolTable<P> table = new SymbolTable<P>(this, node);
		childTables.add(table);
		items.add(new SymbolTableNode<P>(table));
		return table;
	}

	/**
	 * add a symbol to the symbol tree
	 * @param variable		adds a symbol to the symbol table
	 * @param data			the data for the variable
	 */
	public void addSymbol(String variable, P data) {
		if (scopedSymbols.containsKey(variable))
			throw new IllegalStateException(String.format("Symbol: %s already exists", variable));
		
		scopedSymbols.put(variable, data);
		items.add(new SymbolTableNode<P>(variable, data));
	}
	
	/**
	 * @return		the children symbol tables of this symbol table
	 */
	public List<SymbolTable<P>> getChildren() {
		return childTables;
	}
	
	/**
	 * returns true if there is a symbol matching the variable string specified
	 * @param variable		the variable to match
	 * @param localOnly		if true, only check for symbol in current scope
	 * @return				returns true if the symbol is contained
	 */
	public boolean containsSymbol(String variable, boolean localOnly) {
		return scopedSymbols.containsKey(variable) || (!localOnly && parent != null && parent.containsSymbol(variable, false));
	}
	
	/**
	 * returns all symbol data for this particular scope
	 * @return		mapping of symbols to their respective data for this scope
	 */
	public Map<String,P> getAllSymbolData() {
		return scopedSymbols;
	}

	/**
	 * the data for the specified variable
	 * @param variable		the variable to find
	 * @return				the data for this symbol
	 */
	public P getSymbolData(String variable) {
		P data = scopedSymbols.get(variable);
		if (data != null)
			return data;
		
		if (parent != null)
			return parent.getSymbolData(variable);
		
		return null;
	}

	/**
	 * @return		the AST Node for this symbol table 
	 */
	public ASTNode getNode() {
		return node;
	}
	
	
	/**
	 * generates a string of the tree of this symbol table with applicable scopes as branches
	 * @param loadStringify		the means to convert the payload to a string to display in the tree 
	 * @return					the generated tree
	 */
	public String generateTreeString(Func<P,String> loadStringify) {
		SymbolTableNode<P> node = new SymbolTableNode<P>(this);
		
		Func<SymbolTableNode<P>, List<SymbolTableNode<P>>> childrenGetter = (s) -> {
			if (s.isSymbol())
				return null;
			else
				return s.getSymTable().items;
		};
		
		Func<SymbolTableNode<P>, String> stringify = (s) -> {
			if (s.isSymbol())
				return String.format("Variable %s: %s", s.getSymbol(), loadStringify.convert(s.getSymbolData()));
			else
				return String.format("Scope Node %s:", s.getSymTable().getNode().getExpressionType().getType());
		};
		
		return Utils.getTreeString(node, childrenGetter, stringify, false);
	}
	
	/**
	 * generates a symbol table based on the root AST Node
	 * @param root				the root ast node
	 * @param scopeParents		any items signifying expressions that require a new scope
	 * @param symbols			any items signifying means of creating new symbols
	 * @return					the generated symbol table
	 */
	public static <P> SymbolTable<P> generate(ASTNode root, Iterable<ScopeParent> scopeParents, Iterable<Symbol<P>> symbols) {
		SymbolTable<P> symbolTable = new SymbolTable<>(null, root);
		generate(symbolTable, root, 
			StreamSupport.stream(scopeParents.spliterator(), false).collect(Collectors.toMap((sp) -> sp.getExpressionType(), (sp) -> sp)), 
			StreamSupport.stream(symbols.spliterator(), false).collect(Collectors.toMap((s) -> s.getExpressionType(), (s) -> s)));
		
		return symbolTable;
	}
	
	/**
	 * recursive helper for generating a symbol table
	 * @param table			the table to populate
	 * @param root			the ast node to use for this symbol table
	 * @param scopeParents	a mapping of expression types to scope parents
	 * @param symbols		a mapping of expression types to symbols
	 */
	private static <P> void generate(SymbolTable<P> table, ASTNode root, Map<String, ScopeParent> scopeParents, Map<String, Symbol<P>> symbols) {		
		for (ASTComponent comp : root.getComponents()) {
			if (!(comp instanceof ASTNode))
				continue;
			
			ASTNode node = (ASTNode)comp;
			String type = node.getExpressionType().getType();
		
			if (symbols.containsKey(type)) {
				Symbol<P> symbol = symbols.get(type);
				// TODO handle already in mapping
				Tuple<String, P> extracted = symbol.extractPayload(node);
				
				if (extracted != null)
					table.addSymbol(extracted.getItem1(), extracted.getItem2());
			}
			
			SymbolTable<P> childTable;
			ASTNode childNode;
			if (scopeParents.containsKey(type)) {
				ScopeParent scopeParent = scopeParents.get(type);
				childNode = scopeParent.getBody(node);
				childTable = table.addScope(node);
			}
			else {
				childTable = table;
				childNode = node;
			}
			
			generate(childTable, childNode, scopeParents, symbols);
		}
		
	}
}
