package com.gdicristofaro.symboltable;

import java.io.Serializable;

import com.gdicristofaro.ast.ASTNode;
import com.gdicristofaro.ast.Func;

/**
 * ASTNode types that signify the beginning of a scope (i.e. variables declared within a while loop only exist within that while loop)
 */
public class ScopeParent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3993334083764966201L;
	
	private final String expressionType;
	private final Func<ASTNode, ASTNode> bodyStatements;
	
	/**
	 * main constructor
	 * @param expressionType		the signifying expression type for this node (i.e. for <whilestatement> ::= ... 'whilestatement' is the expression type
	 * @param bodyStatements		the function to obtain the body statements given the parent node
	 */
	public ScopeParent(String expressionType, Func<ASTNode, ASTNode> bodyStatements) {
		this.expressionType = expressionType;
		this.bodyStatements = bodyStatements;
	}

	/**
	 * @return		the expression type of this item
	 */
	public String getExpressionType() {
		return expressionType;
	}

	/**
	 * returns the body statements given the parent node following this rule
	 * @param parent		the ast node with the same expression type as 'getExpressionType'
	 * @return			the node representing the body statements for this node
	 */
	public ASTNode getBody(ASTNode parent) {
		if (!parent.getExpressionType().getType().equals(expressionType))
			throw new IllegalStateException(String.format("Node is of type %s and not of expected type %s.", parent.getExpressionType().getType(), expressionType));
		
		return bodyStatements.convert(parent);
	}
}
