package com.gdicristofaro.ast;

import java.io.Serializable;

public interface Func<I,O> extends Serializable {
	O convert(I input);
}
