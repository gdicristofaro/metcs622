package com.gdicristofaro.ast;

/**
 * represents a leaf in the abstract syntax tree with a specified string literal
 */
public class ASTLiteral extends ASTComponent {
	private final String value;
	
	/**
	 * main constructor
	 * @param stringIndex	the index of this literal in the original string 
	 * @param value		the literal value of this abstract syntax tree leaf
	 */
	public ASTLiteral(int stringIndex, String value) {
		super(stringIndex);
		this.value = value;
	}

	/**
	 * @return		the string value of this literal		
	 */
	public String getValue() {
		return value;
	}

	@Override
	public String getDisplayString() {
		String val = (getValue() == null) ? "" : getValue();
		return "\"" + val.replace(System.getProperty("line.separator"), "") + "\"";
	}

	@Override
	public ASTComponent[] getComponents() {
		return null;
	}
}
