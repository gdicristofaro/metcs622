package com.gdicristofaro.ast;

/**
 * represents an expression in a particular gramma
 */
public class Expression {
	private final ExpressionChoice[] choices;
	private final String type;
	
	/**
	 * the main constructor
	 * @param choices		the choices for this particular expression
	 * @param type			the identifier of this expression
	 */
	public Expression(ExpressionChoice[] choices, String type) {
		this.choices = choices;
		this.type = type;
	}

	/**
	 * @return			the choices for this particular expression
	 */
	public ExpressionChoice[] getChoices() {
		return choices;
	}

	/**
	 * @return			the choices for this particular expression
	 */
	public String getType() {
		return type;
	}
}
