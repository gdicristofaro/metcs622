package com.gdicristofaro.ast;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
	// taken from http://www.mkyong.com/java/java-how-to-split-string-by-new-line/
	static String newLine = "\\r?\\n";
	
	/**
	 * translates an index in the string to a line number and column
	 * @param s			the string
	 * @param index		the index in the string
	 * @return			a tuple of the form (line number, column number) or null if index is outside of string bounds
	 */
	public static Tuple<Integer, Integer> IndexToLineCol(String s, int index) {
		if (index < 0 || index >= s.length())
			return null;
		
		s = s.substring(0, index + 1);
		
		Matcher m = Pattern.compile(newLine).matcher(s);
		
		int lineNumber = 1;
		int lineBreakIndex = 0;
		while (m.find()) {
			lineNumber++;
			lineBreakIndex = m.end();
		}
		
		return new Tuple<Integer, Integer>(lineNumber, index - lineBreakIndex + 1);
	}
	

	
	public static <T> String getTreeString(T item, Func<T,List<T>> getChildren, Func<T,String> stringify, boolean singleChildSimplify) {
		return getTreeString(item, getChildren, stringify, singleChildSimplify, "","");
	}

	
	protected static <T> String getTreeString(T item, Func<T,List<T>> getChildren, Func<T,String> stringify, 
											boolean singleChildSimplify, String thisPrefix, String childrenPrefix) {
		
		List<T> children = getChildren.convert(item);
		if (singleChildSimplify && children != null && children.size() == 1)
			return getTreeString(children.get(0), getChildren, stringify, singleChildSimplify, thisPrefix, childrenPrefix);
		
		String stringified = stringify.convert(item);
		String toRet = thisPrefix + (stringified == null ? "" : stringified);
		
		if (children != null) {
			for (int i = 0; i < children.size(); i++) {
				T child = children.get(i);
				if (child == null)
					continue;
				
				String treeBranch = (i == children.size() - 1) ? "└──" : "├──";
				String descendent = (i == children.size() - 1) ? "   " : "│  ";
				toRet += System.getProperty("line.separator") + 
					getTreeString(child, getChildren, stringify, singleChildSimplify, childrenPrefix + treeBranch, childrenPrefix + descendent);
			}
		}
		
		return toRet;
	}
}
