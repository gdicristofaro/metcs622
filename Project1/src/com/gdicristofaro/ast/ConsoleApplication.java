package com.gdicristofaro.ast;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import com.gdicristofaro.dao.Dao;
import com.gdicristofaro.dao.Dao.RunInfo;
import com.gdicristofaro.symboltable.DefaultSymbols;
import com.gdicristofaro.symboltable.DefaultSymbols.SymbolPayload;
import com.gdicristofaro.symboltable.Symbol;
import com.gdicristofaro.symboltable.SymbolTable;
import com.gdicristofaro.symboltable.SymbolTableSerialization;
import com.gdicristofaro.symboltable.SymbolTableSerialization.SerializationDTO;

public class ConsoleApplication {
	/**
	 * prints a string to output
	 * @param s		the string to output
	 */
	private static void print(String s) {
		System.out.println(s);
	}
	
	/**
	 * prints a formatted string
	 * @param s			the string to print
	 * @param params		the parameters to print using string format on s
	 */
	private static void print(String s, Object...params) {
		System.out.println(String.format(s, params));
	}
	
	/**
	 * reads in a grammar specified as the first argument and the content file specified as the second argument
	 * @param args 		command line arguments in the form of [path to grammar file] [path to content file] [path to output file] 
	 * [-s for symbol table] [symbol table parsing file (optional)] [-l logs ast and symbol table, if provided, in database]
	 */
	public static void main(String[] args) {
		
		// ensure that three arguments are provided.  if not, assume default arguments
		if (args == null || args.length < 1) {
			args = new String[] {
				"fullgrammar.txt", 
				"scopecontent.txt",
				"output.txt",
				"-s",
				"symboltable.dat",
				"-l"};
			
			args = new String[] {"METRICS"};
			//args = new String[] {"LOG"};
		}	
		
		
		try {
			if (args.length == 1 && "METRICS".compareTo(args[0]) == 0) {
				printMetrics();
			}
			else if (args.length == 1 && "LOG".compareTo(args[0]) == 0) {
				printLog();
			}
			else if (args.length >= 3) {
				String contentFile = args[1];
				// reads in the content file 
				String contentFileString = readFile(contentFile);
				if (contentFileString == null)
					throw new IllegalStateException("file at: " + contentFile + " could not be read.");
				
				ASTNode rootNode = parse(args[0], contentFileString);
				SymbolTableResult<?> symbolTable = getSymbolTable(rootNode, args);
				
				if (getFlag(args, "-l") > 2) {
					logRun(args, rootNode, symbolTable);
				}
				
				writeToOutput(contentFileString, args[2], rootNode, symbolTable);
			}
			else {
				print("Unkown command line choice: " + String.join(" ", args));
				print("METRICS for database metrics");
				print("LOG for database run log");
				print("OR [path to grammar file] [path to content file] [path to output file] \n" + 
					"[-s for symbol table] [symbol table parsing file (optional)] [-l logs ast and symbol table, if provided, in database]");
			}
			

		}
		catch (Exception e) {
			print(e.getMessage());
		}
	}
	
	/**
	 * prints a log of all runs
	 */
	private static void printLog() {
		print("----- ALL PROGRAM RUNS -----");
		List<RunInfo> inf = Dao.getRuns();
		for (RunInfo i : inf)
			print("Run on content file: %s grammar file: %s with output file: %s at %s with %d ast nodes and %d symbols", 
				i.getContentFile(), i.getGrammarFile(), i.getOutputFile(), i.getTime(), i.getAstnodes(), i.getSymbols());
	}

	/**
	 * prints metrics concerning run information
	 */
	private static void printMetrics() {
		print("----- RUN METRICS -----");
		print("Variables are at %s scope depth on average", Dao.averageScopeDepth());
		print("");
		print("The most common scope parents are:");
		for (String parent : Dao.mostCommonScopeParent())
			print("%s", parent);
		
		print("");
		print("The most common variable names are:");
		for (String var : Dao.mostCommonVariables())
			print("%s", var);
	}

	/**
	 * gets the index of a flag in an args array
	 * @param args		the command line args
	 * @param flag		the flag to search for
	 * @return			the index of the flag or -1 if not found
	 */
	private static int getFlag(String[] args, String flag) {
		for (int i = 0; i < args.length; i++)
			if (args[i].toUpperCase().equals(flag.toUpperCase()))
				return i;
		
		return -1;
	}
	
	/**
	 * parses the content file string with the grammar file 
	 * @param grammarFile			the grammar file location
	 * @param contentFileString		the content of the content file
	 * @return						the root ASTNode
	 */
	private static ASTNode parse(String grammarFile, String contentFileString) {
		// read the grammar file in
		String grammarFileString = readFile(grammarFile);
		// if unsuccessful, don't continue
		if (grammarFileString == null)
			throw new IllegalStateException("file at: " + grammarFile + " could not be read.");
		
		// convert the grammar into a recursive descent parser object to be utilized with the content
		Expression leadExp = GrammarParser.parseGrammar(grammarFileString);
		RecursiveDescentParser parser = new RecursiveDescentParser(leadExp);
		
		// attempts to parse the file into an AST reporting errors that might occur.
		ParseResult res = parser.parse(contentFileString);
		if (!res.isSuccess()) {
			Tuple<Integer,Integer> pos = Utils.IndexToLineCol(contentFileString, res.getErrorPosition());
			throw new IllegalStateException("There was an error parsing content file: " + res.getErrorMessage() + " at line number: " + pos.getItem1() + " column: " + pos.getItem2());
		}
		
		return (ASTNode)res.getParsed();
	}
	
	/**
	 * retrieves the symbol table for the root node if arguments support it
	 * @param rootNode		the root node 
	 * @param args			the command line arguments
	 * @return				the generated symbol table result; or null if no symbol table parsed
	 */
	@SuppressWarnings("unchecked")
	private static SymbolTableResult<?> getSymbolTable(ASTNode rootNode, String[] args) {
		// TODO this could potentially be generalized to a serializable object loaded in
		int sFlag = getFlag(args, "-s");
		
		// if -s flag is not past the initial arguments
		if (sFlag < 2)
			return null;
		
		SerializationDTO<?> symTableDTO = null;
		// if location for object file is specified, use that
		if (sFlag < args.length - 1 && !args[sFlag + 1].startsWith("-")) {
			try {
				symTableDTO = SymbolTableSerialization.deserialize(args[sFlag + 1]);
			}
			catch (Exception e) {
				print("WARNING: unable to deserialize object file located at " + args[sFlag + 1] + ".  Using defaults instead.");
			}
		}

		if (symTableDTO == null)
			// otherwise use default symbols
			symTableDTO = new SerializationDTO<>(
					(Symbol<SymbolPayload>[]) new Symbol[]{ DefaultSymbols.VARIABLE_DECLARATION }, 
					DefaultSymbols.DEFAULT_SCOPE_PARENTS,
					(p) -> String.format("{ type: %s, initialized: %b }", p.getType(), p.isInitialized()));

		return generateSymbolTableResult(rootNode, symTableDTO);
	}
	
	
	/**
	 * the result of a symbol table
	 * @param <T>		the payload type
	 */
	static class SymbolTableResult<T> {
		private final SymbolTable<T> table;
		private final Func<T,String> payloadConverter;
		private final String symbolTableTree;
		
		/**
		 * main constructor
		 * @param table				the symbol table that was generated
		 * @param payloadConverter	the payload converter
		 * @param symbolTableTree	the string of the symbol table
		 */
		public SymbolTableResult(SymbolTable<T> table, Func<T, String> payloadConverter, String symbolTableTree) {
			this.table = table;
			this.payloadConverter = payloadConverter;
			this.symbolTableTree = symbolTableTree;
		}
		public SymbolTable<T> getTable() {
			return table;
		}
		public Func<T, String> getPayloadConverter() {
			return payloadConverter;
		}
		public String getSymbolTableTree() {
			return symbolTableTree;
		}
	}
	
	
	/**
	 * derives symbol table hierarchy based on astnode root and the symbol table
	 * @param root		the root ast node
	 * @param symTable	the symbol table object for determining the symbol table
	 * @return			return a string with the hierarchical string representing the symbol table
	 */
	private static <T> SymbolTableResult<T> generateSymbolTableResult(ASTNode root, SerializationDTO<T> symTable) {
		if (symTable == null)
			return null;
		
		SymbolTable<T> symbolTable = SymbolTable.generate(root, 
			Arrays.asList(symTable.getScopeItems()),
			Arrays.asList(symTable.getSymbols()));
		
		String s = symbolTable.generateTreeString(symTable.getPayloadStringify());
		return new SymbolTableResult<T>(symbolTable, symTable.getPayloadStringify(), s);
	}

	/**
	 * writes content to output
	 * @param contentFileString		the content file string
	 * @param outputPath				the output path
	 * @param rootNode				the root ast node
	 * @param symTable				the symbol table result to print
	 */
	private static void writeToOutput(String contentFileString, String outputPath, ASTNode rootNode, SymbolTableResult<?> symTable) {
		String symTableString = "";
		if (symTable != null) {
			symTableString = "----- SYMBOL TABLE: -----" +
				System.lineSeparator() +
				symTable.getSymbolTableTree() + 
				System.lineSeparator() +
				System.lineSeparator(); 
		}

		try {
			PrintWriter out = new PrintWriter(outputPath);
			out.write("----- INPUT: -----" +
				System.lineSeparator() +
				contentFileString + 
				System.lineSeparator() +
				System.lineSeparator() +
				symTableString +
				"----- AST: -----" +
				System.lineSeparator() +
				rootNode.getTreeString(true));
			out.close();
		}
		catch (Exception e) {
			System.out.println("There was an error writing to the output file: " + e.getMessage());
		}
	}

	/**
	 * logs a run in the database
	 * @param args		the command line arguments
	 * @param parsed		the parsed root ast node
	 * @param table		the symbol table result if parsed (null if not created)
	 */
	private static <T> void logRun(String[] args, ASTNode parsed, SymbolTableResult<T> table) {
		Dao.insertData(getFileName(args[1]), getFileName(args[0]), getFileName(args[2]), 
						parsed, table.getTable(), table.getPayloadConverter());
	}
	
	/**
	 * retrieves a file name from the string of the path
	 * @param s		the path
	 * @return		the file name
	 */
	private static String getFileName(String s) {
		return new File(s).getName();
	}

	/**
	 * reads the file to a string
	 * @param path		the path of the file to read
	 * @return			the string of that file
	 */
	public static String readFile(String path) {
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, StandardCharsets.UTF_8);
		} catch (IOException e) {
			print("There was an error reading file: " + path + ".");
			return null;
		}
	}
}
