package com.gdicristofaro.ast;

/**
 * a simple tuple class
 * 
 * @param <T1>	type of the first object
 * @param <T2>	type of the second object
 */
public class Tuple<T1,T2> {
	private final T1 item1;
	private final T2 item2;
	
	/**
	 * main constructor
	 * @param item1		the first item
	 * @param item2		the second item
	 */
	public Tuple(T1 item1, T2 item2) {
		this.item1 = item1;
		this.item2 = item2;
	}

	/**
	 * 
	 * @return	the first item
	 */
	public T1 getItem1() {
		return item1;
	}

	/**
	 * 
	 * @return	the second item
	 */
	public T2 getItem2() {
		return item2;
	}

	// eclipse generated hashcode for lookup purposes in a hashmap
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item1 == null) ? 0 : item1.hashCode());
		result = prime * result + ((item2 == null) ? 0 : item2.hashCode());
		return result;
	}

	// eclipse generated equals for lookup purposes
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		Tuple other = (Tuple) obj;
		if (item1 == null) {
			if (other.item1 != null)
				return false;
		} else if (!item1.equals(other.item1))
			return false;
		if (item2 == null) {
			if (other.item2 != null)
				return false;
		} else if (!item2.equals(other.item2))
			return false;
		return true;
	}
}
