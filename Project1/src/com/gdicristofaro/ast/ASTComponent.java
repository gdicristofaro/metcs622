package com.gdicristofaro.ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * represents a component (branch or leaf) in an abstract syntax tree 
 */
public abstract class ASTComponent {
	/**
	 * original BNF defines lists in a linked list format.  this translates those linked list formats into a java list format 
	 * for ease of use.  assumes the list has bnf similar to <node> <lst> | <node> where the last child is of type node
	 * 
	 * @param rootComp			the root component of the list			
	 * @param listChoice			the index of the choice in the list expression that represents a continuation of the list 
	 * @param componentIndex		if it is the list choice, the index of the token that represents the actual item
	 * @param nextIndex			if it is the list choice, the index of the token that represents the next list item
	 * @return					a list of the ASTComponents the represent items in the list
	 */
	public static List<ASTComponent> convertASTList(ASTComponent rootComp, int listChoice, int componentIndex, int nextIndex) {
		return foldLeft((r, cur) -> {
			if (cur.getComponents() != null && cur.getComponents().length > componentIndex)
				r.add(cur.getComponents()[componentIndex]);
			else if (cur.getStringIndex() != listChoice)
				r.add(cur);

			return r;
		},
		new ArrayList<ASTComponent>(),
		rootComp, listChoice, nextIndex);
	}

	/**
	 * performs a left fold on the ast linked list
	 * @param folder			the left fold operator
	 * @param initial		initial value
	 * @param parent			the parent for the linked list
	 * @param listChoice		the list choice in the linked list (getStringIndex())
	 * @param nextIndex		the getComponents()[nextIndex] is the reference to the next list node
	 * @return				the result
	 */
	public static <R> R foldLeft(Func2<R,ASTComponent,R> folder, R initial, ASTComponent parent, int listChoice, int nextIndex) {
		if (parent == null)
			throw new IllegalArgumentException("a parent ast component should be specified");

		if (listChoice < 0 || nextIndex < 0)
			throw new IllegalArgumentException("indexes and choices must be >= 0");
		
		if (!(parent instanceof ASTNode))
			return initial;
		
		ASTNode root = (ASTNode) parent;
		R curVal = initial;
		
		
		while (root != null && root.getExpressionChoiceIndex() == listChoice) {
			curVal = folder.convert(curVal, root);
			
			// go to next expression if applicable (if the choice is the syntax for an addition row
			if (root.getExpressionChoiceIndex() == listChoice && 
				root.getComponents().length > nextIndex && 
				root.getComponents()[nextIndex] instanceof ASTNode)
				root = (ASTNode)root.getComponents()[nextIndex];
			else
				root = null;
		}
		
		// in the instance that this is the leaf, pass that along as well
		if (root != null)
			curVal = folder.convert(curVal, root);

		return curVal;		
	}
	
	

	public static <R> R foldLeft(Func2<R,ASTComponent,R> folder, R initial, ASTNode root, int nextIndex) {
		if (root == null)
			throw new IllegalArgumentException("a parent ast component should be specified");

		String parentType = root.getExpressionType().getType();
		R curVal = initial;
		
		while (root != null) {
			curVal = folder.convert(curVal, root);
			
			ASTComponent next = root.getChild(parentType);
			if (next == null)
				return curVal;
			else
				root = (ASTNode) next;
		}
		return curVal;		
	}

	
	
	
	
	private final int stringIndex;
	
	
	/**
	 * main constructor for ASTComponent abstract class
	 * @param stringIndex		the index within the original string that this component begins
	 */
	public ASTComponent(int stringIndex) {
		this.stringIndex = stringIndex;
	}
	
	
	/**
	 * extract value as string of component
	 * @return	the return string
	 */
	public String extractVal() {
		if (this instanceof ASTLiteral) {
			return ((ASTLiteral) this).getValue();
		}
		else {
			return String.join("", (Iterable<String>) Arrays.stream(getComponents()).map(c -> c.extractVal())::iterator);
		}
	}
	
	
	/**
	 * @return	the index within the original string that this component begins
	 */
	public int getStringIndex() {
		return stringIndex;
	}

	/**
	 * @param singleChildSimplify	in the instance that an AST Node has precisely one child, print the child in replacement of the parent
	 * @return	returns a string with an ASCII tree representation of the tree starting at this component 
	 */
	public String getTreeString(boolean singleChildSimplify) {
		return Utils.getTreeString(this, (c) -> {
			if (c.getComponents() == null)
				return null;
			else
				return Arrays.asList(c.getComponents());
		}, 
		(c) -> c.getDisplayString(), singleChildSimplify);
	}
	
	
	/**
	 * @return		the string to display for this component
	 */
	public abstract String getDisplayString();
	
	/**
	 * @return		any children belonging to this component; null if an AST Literal leaf node
	 */
	public abstract ASTComponent[] getComponents();
}
