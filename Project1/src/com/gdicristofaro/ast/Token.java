package com.gdicristofaro.ast;

/**
 * represents one token that is a piece of an expression choice
 * for instance, with bnf like:
 * 
 * <exp> ::= 'lit1' <exp2> 'lit2' | <exp3>
 * 
 * lit1 is a token as are exp2, lit2 and exp3
 */
public interface Token {
	/**
	 * determines whether or not this token matches the string at the given position
	 * @param s			the string to be parsed
	 * @param curIndex	the current index of the parser
	 * @param parser		the parser (used in instances where the token needs recursive parsing on another expression)
	 * @return			the result of parsing (successful or unsuccessful); see ParseResult for more information
	 */
	public ParseResult handle(String s, int curIndex, RecursiveDescentParser parser);
}
