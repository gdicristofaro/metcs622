package com.gdicristofaro.ast;

/**
 * represents the result of parsing a string based on an expression
 */
public class ParseResult {
	private final boolean success;
	
	private final ASTComponent parsed;
	private final int endParsePosition;
	
	private final int errorPosition;
	private final String errorMessage;
	
	/**
	 * main constructor of a ParseResult
	 * @param success			whether or not the parsing was successful
	 * @param parsed				the parsed ASTComponent object
	 * @param endParsePosition	the end position of parsing; the ASTComponent represents up to but not 
	 * 							including endParsePosition in the string 
	 * @param errorPosition		the index in the string of the error
	 * @param errorMessage		an error message specifying which expression could not be parsed appropriately
	 */
	public ParseResult(boolean success, ASTComponent parsed, int endParsePosition, int errorPosition,
			String errorMessage) {
		this.success = success;
		this.parsed = parsed;
		this.endParsePosition = endParsePosition;
		this.errorPosition = errorPosition;
		this.errorMessage = errorMessage;
	}

	/**
	 * @return	whether or not the operation was successful
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @return	if the parse operation was successful, returns the parsed AST Component
	 */
	public ASTComponent getParsed() {
		return parsed;
	}

	/**
	 * @return	if the parse operation was successful, returns the end position of parsing; 
	 * 			the ASTComponent represents up to but not including endParsePosition in the string 
	 */
	public int getEndParsePosition() {
		return endParsePosition;
	}

	/**
	 * @return	if the parse operation was unsuccessful, returns the index of the string of the error
	 * 			or -1 if not specified
	 */
	public int getErrorPosition() {
		return errorPosition;
	}

	/**
	 * @return	if the parse operation was unsuccessful, returns the error message or null if not 
	 * 			specified
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
}

