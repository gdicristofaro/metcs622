package com.gdicristofaro.ast;

import java.io.Serializable;

public interface Func2<I1,I2,O> extends Serializable {
	O convert(I1 input1, I2 input2);
}
