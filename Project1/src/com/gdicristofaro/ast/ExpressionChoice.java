package com.gdicristofaro.ast;

/**
 * represents a possible option for an expression 
 * i.e. within a grammar like:
 * exp ::= exp1 | exp2 lit1 exp3
 * 
 * 'exp1' as well as 'exp2 lit1 exp3' represent choices
 */
public class ExpressionChoice {
	private final Token[] tokens;
	private final int index;

	/**
	 * main constructor
	 * @param index		the index of this choice within the expression in the original grammar
	 * 					(order might be changed in expression object for efficiency)
	 * @param tokens		the tokens required for this choice
	 */
	public ExpressionChoice(int index, Token[] tokens) {
		this.tokens = tokens;
		this.index = index;
	}

	/**
	 * @return		the tokens required for this choice
	 */
	public Token[] getTokens() {
		return tokens;
	}
	
	/**
	 * @return		the index of this choice within the expression in the original grammar
	 * 				(order might be changed in expression object for efficiency)
	 */
	public int getIndex() {
		return index;
	}
}
