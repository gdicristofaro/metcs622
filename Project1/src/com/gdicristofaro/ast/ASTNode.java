package com.gdicristofaro.ast;

/**
 *	represents a node in an abstract syntax tree
 */
public class ASTNode extends ASTComponent {
	private final Expression expressionType;
	private final int expressionChoiceIndex;
	
	private final ASTComponent[] components;
	
	/**
	 * main constructor
	 * @param stringIndex			the index within the original string that this component begins
	 * @param expressionType			the type of the expression associated with this node (i.e. ifstatement)
	 * @param expressionChoiceIndex	the index of the expression choice within 
	 * @param components				the children of this node
	 */
	public ASTNode(int stringIndex, Expression expressionType, int expressionChoiceIndex, ASTComponent[] components) {
		super(stringIndex);
		this.expressionType = expressionType;
		this.expressionChoiceIndex = expressionChoiceIndex;
		this.components = components;
	}

	/**
	 * @return 	the expression associated with this node
	 */
	public Expression getExpressionType() {
		return expressionType;
	}

	/**
	 * @return	the index of the choice within that expression for this particular parsing
	 */
	public int getExpressionChoiceIndex() {
		return expressionChoiceIndex;
	}

	/**
	 * @return	the children of this node
	 */
	public ASTComponent[] getComponents() {
		return components;
	}

	@Override
	public String getDisplayString() {
		if (getExpressionType() == null || getExpressionType().getType() == null)
			return "";
		
		return getExpressionType().getType().replace(System.getProperty("line.separator"), "");
	}
	
	public ASTComponent getChild(String expressionType) {
		return getChild(expressionType, 0);
	}
	
	public ASTComponent getChild(String expressionType, int typeIndex) {
		int found = 0;
		for (ASTComponent c : getComponents()) {
			if (!(c instanceof ASTNode))
				continue;
			
			ASTNode nd = (ASTNode) c;
			if (nd.getExpressionType().getType().equals(expressionType)) {
				if (found == typeIndex)
					return nd;
				else
					found++;
			}
		}
		
		return null;
	}
}
