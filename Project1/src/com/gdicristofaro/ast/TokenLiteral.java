package com.gdicristofaro.ast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * specifies a token in an expression which has no nested elements
 */
public class TokenLiteral implements Token {
	private final String regex;
	private final Pattern pattern;
	
	/**
	 * main constructor
	 * @param regex		regex to be matched in the string to satisfy this token
	 */
	public TokenLiteral(String regex) {
		if (regex != null && regex.length() > 0) {
			this.regex = regex;
			// ensures that only items found at beginning of string are matched
			this.pattern = Pattern.compile("\\A" + regex);	
		}
		else {
			this.regex = "";
			this.pattern = null;
		}
	}
	
	/**
	 * @return	the regex pattern to be matched for this token
	 */
	public String getRegex() {
		return regex;
	}

	@Override
	public ParseResult handle(String s, int curIndex, RecursiveDescentParser parser) {
		if (curIndex < 0 || curIndex > s.length())
			return new ParseResult(false, null, -1, curIndex, "current index of " + s.length() + 
				" is outside of string's bounds with length of " + s.length());
		
		// in the event that the regex is null or empty, pass this token back as satisfied
		if (regex.length() == 0)
			return new ParseResult(true, new ASTLiteral(curIndex, ""), curIndex, -1, null);

		// specifies a substring where the start is the current index
		String subS = s.substring(curIndex);
		Matcher m = pattern.matcher(subS);
		if (m.find()) {
			ASTComponent literal = new ASTLiteral(curIndex, subS.substring(m.start(), m.end()));
			return new ParseResult(true, literal, curIndex + m.end(), -1, null);
		}
		else {
			return new ParseResult(false, null, -1, curIndex, null);
		}
	}
}
