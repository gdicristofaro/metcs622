package com.gdicristofaro.ast;

/**
 * implements a recursive descent parser
 */
public class RecursiveDescentParser {
	private final Expression leadExpression;
	
	/**
	 * main constructor
	 * @param leadExpression		represents the top most parent expression in the grammar
	 */
	public RecursiveDescentParser(Expression leadExpression) {
		this.leadExpression = leadExpression;
	}
	
	/**
	 * parses the string into the grammar specified for this parser
	 * @param s		the string to parse
	 * @return		the parse result (successful or unsuccessful) utilizing the specified grammar
	 */
	public ParseResult parse(String s) {
		return parse(s,0, leadExpression);
	}
	
	/**
	 * parses the string starting at the specified expression using the specified grammar expression 
	 * @param s			the string to parse
	 * @param index		the index at which to start for parsing
	 * @param curExp		the expression to use as the context for parsing
	 * @return			the parse result (successful or unsuccessful) utilizing the specified grammar
	 */
	public ParseResult parse(String s, int index, Expression curExp) {
		ExpressionChoice[] choices = curExp.getChoices();
		
		// set up error message here but discard if a better more precise message can be determined
		int errorIndex = index;
		String errorMessage = String.format("Unable to parse %s", curExp.getType());
		int mostTokensMatched = 0;
		
		// TODO backtracking when one choice is a subdomain of another could be optimized here
		// iterate through the choices taking the first matched choice possible
		for (int c = 0; c < choices.length; c++) {
			ExpressionChoice choice = choices[c];
			
			Token[] tokens = choice.getTokens();
			ASTComponent[] components = new ASTComponent[tokens.length];
			
			// represents the current index for the next token to be parsed
			int curPos = index;
			boolean successful = true;
			
			// iterate through the tokens checking to see if they match the string 
			for (int i = 0; i < tokens.length; i++)
			{
				Token token = tokens[i];
				ParseResult result = token.handle(s, curPos, this);
				
				if (result.isSuccess()) {
					components[i] = result.getParsed();
					curPos = result.getEndParsePosition();
				}
				// if a token is not successful, this choice will not work
				else {
					successful = false;
					
					// to get precise error messages, the error for this token will be reported if
					// the error message and index are set and this expression choice has more tokens 
					// matched than previous choices
					if (result.getErrorMessage() != null && result.getErrorPosition() >= 0 && 
							i > mostTokensMatched) {
						errorIndex = result.getErrorPosition();
						errorMessage = result.getErrorMessage();
						mostTokensMatched = i;
					}
					
					break;
				}
			}
			
			// if successful, return the parsed ast node
			if (successful) {
				ASTNode node = new ASTNode(index, curExp, choices[c].getIndex(), components);
				return new ParseResult(true, node, curPos, -1, null);
			}
		}
		
		// if no successful choices, report the error and continue
		return new ParseResult(false, null, -1, errorIndex, errorMessage);
	}

}
