package com.gdicristofaro.ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * responsible for parsing of a grammar for the use of parsing strings into an abstract syntax tree
 */
public class GrammarParser {
	static final Expression RootExpression;
	static final RecursiveDescentParser Parser;
	
	// based on https://stackoverflow.com/questions/249791/regex-for-quoted-string-with-escaping-quotes
	// literals will be between single quotes; two single quotes together will be treated as one single quote 
	static String singleQuote = "'(?:[^']|'')*'";
	
	static String bracketed = "<(.*?)>";
	static String whiteSpace = "[ \\t]+";
	static String whiteSpaceOption = "[ \\t]*";
	
	// taken from http://www.mkyong.com/java/java-how-to-split-string-by-new-line/
	static String newLine = "\\r?\\n";
	
	
	// this sets up the basic grammar for ToyParser's specific interpretation of BNF grammar
	// so that grammars can be parsed utilizing the expressions set forth here
	static {
		// represents a collection of bnf expressions
		ExpressionChoice[] collectionChoices = new ExpressionChoice[2];
		Expression ExpressionCollection = new Expression(collectionChoices, "ExpressionCollection");
		
		// represents a bnf expression
		ExpressionChoice[] expChoices = new ExpressionChoice[1];
		Expression Expression = new Expression(expChoices, "Expression");
		
		// represents choices for a bnf expression
		ExpressionChoice[] choicesChoices = new ExpressionChoice[2];
		Expression Choices = new Expression(choicesChoices, "Choices");
		
		// represents a choice in choices in a bnf expression
		ExpressionChoice[] choiceChoices = new ExpressionChoice[2];
		Expression Choice = new Expression(choiceChoices, "Choice");
		
		// represents a token (literal or expression reference) in a choice in a bnf expression
		ExpressionChoice[] tokenChoices = new ExpressionChoice[2];
		Expression Token = new Expression(tokenChoices, "Token");
		
		
		collectionChoices[0] = new ExpressionChoice(0, new Token[] {new TokenExpression(Expression), new TokenLiteral(whiteSpaceOption + newLine), new TokenExpression(ExpressionCollection)});
		collectionChoices[1] = new ExpressionChoice(1, new Token[] {new TokenExpression(Expression)});
		
		expChoices[0] = new ExpressionChoice(0, new Token[] {new TokenLiteral(bracketed), new TokenLiteral(whiteSpace + "::=" + whiteSpace), new TokenExpression(Choices)});
		
		choicesChoices[0] = new ExpressionChoice(0, new Token[] {new TokenExpression(Choice), new TokenLiteral(whiteSpace + "\\|" + whiteSpace), new TokenExpression(Choices)});
		choicesChoices[1] = new ExpressionChoice(1, new Token[] {new TokenExpression(Choice)});
		
		choiceChoices[0] = new ExpressionChoice(0, new Token[] {new TokenExpression(Token), new TokenLiteral(whiteSpace), new TokenExpression(Choice)});
		choiceChoices[1] = new ExpressionChoice(1, new Token[] {new TokenExpression(Token)});
		
		tokenChoices[0] = new ExpressionChoice(0, new Token[] { new TokenLiteral(bracketed)});
		tokenChoices[1] = new ExpressionChoice(1, new Token[] { new TokenLiteral(singleQuote)});
		
		RootExpression = ExpressionCollection;
		Parser = new RecursiveDescentParser(RootExpression);
	}
	
	
	/**
	 * original BNF defines lists in a linked list format.  this translates those linked list formats into a java list format 
	 * for ease of use
	 * 
	 * @param rootComp			the root component of the list			
	 * @param listChoice			the index of the choice in the list expression that represents a continuation of the list 
	 * @param componentIndex		if it is the list choice, the index of the token that represents the actual item
	 * @param nextIndex			if it is the list choice, the index of the token that represents the next list item
	 * @return					a list of the ASTComponents the represent items in the list
	 */
	public static List<ASTComponent> convertASTList(ASTComponent rootComp, int listChoice, int componentIndex, int nextIndex) {
		if (listChoice < 0 || componentIndex < 0 || nextIndex < 0)
			throw new IllegalStateException("indexes and choices must be >= 0");
		
		List<ASTComponent> components = new ArrayList<ASTComponent>();
		
		if (!(rootComp instanceof ASTNode))
			return components;
		
		ASTNode root = (ASTNode) rootComp;
		while (root != null) {
			if (root.getComponents().length > componentIndex)
				components.add(root.getComponents()[componentIndex]);
			
			// go to next expression if applicable (if the choice is the syntax for an addition row
			if (root.getExpressionChoiceIndex() == listChoice && root.getComponents().length > nextIndex && root.getComponents()[nextIndex] instanceof ASTNode)
				root = (ASTNode)root.getComponents()[nextIndex];
			else
				root = null;
		}
		return components;
	}

	
	/**
	 * parses a grammar into expressions
	 * @param s			the string containing the ToyParser-specified BNF grammar
	 * 					grammars should be specified in the following example:
	 * 					<expression definition> ::= <expression reference> 'regex for literal' | <expression reference for a second choice>			
	 * 					for now, the first expression should represent the overall parent expression by which all other expressions are parsed
	 * 					also for now, choices where one choice is a possible substring of another should have a higher index  
	 * 					(i.e. <number> '\+' <number> | <number>)
	 * @return			the lead expression
	 */
	public static Expression parseGrammar(String s) {
		// parse the syntax into an ast
		ParseResult result = Parser.parse(s);
		
		// if there is an error, account for it
		if (!result.isSuccess()) {
			String errorMessage = "Grammar cannot be parsed";
			if (result.getErrorMessage() != null)
				errorMessage += ". " + result.getErrorMessage();
			
			if (result.getErrorPosition() >= 0) {
				Tuple<Integer, Integer> lineCol = Utils.IndexToLineCol(s, result.getErrorPosition());
				errorMessage += " at line: " + lineCol.getItem1() + " column: " + lineCol.getItem2();
			}
			
			errorMessage += ".";
			
			throw new IllegalStateException(errorMessage);
		}
			
		ASTComponent root = result.getParsed();

		Map<String, Expression> expressions = new HashMap<String, Expression>();
		// mapping of (expression name, choice index) => a list of (token text, is literal)
		Map<Tuple<String,Integer>, List<Tuple<String, Boolean>>> expressionTokens = new HashMap<Tuple<String,Integer>, List<Tuple<String, Boolean>>>();
		
		Expression rootExpression = parseGrammarAST(root, expressions, expressionTokens);
		reconcileExpressions(expressions, expressionTokens);
		
		return rootExpression;
	}

	/**
	 * performs initial step in converting a grammar AST into an expression tree for parsing expressions
	 * expression references are added to the expressionTokens map to later changed to the expression object references
	 * 
	 * @param root				the root AST component
	 * @param expressions		a mapping of expression identifiers to their respective expression objects
	 * @param expressionTokens	a mapping of (expression identifier, choice index) => a list of (token text, whether or not it is literal)
	 * @return					the root expression
	 */
	private static Expression parseGrammarAST(ASTComponent root, 
			Map<String, Expression> expressions,
			Map<Tuple<String, Integer>, List<Tuple<String, Boolean>>> expressionTokens) {
		
		Expression rootExpression = null;
		
		for (ASTComponent expComp : ASTComponent.convertASTList(root, 0, 0, 2)) {
			ASTNode expression = (ASTNode) expComp;
			// expression name should be the first value in the expression
			String expressionName = ((ASTLiteral)expression.getComponents()[0]).getValue();
			// take off brackets for expression name
			expressionName = expressionName.substring(1, expressionName.length() - 1);
			
			// iterate through choices
			List<ASTComponent> choices = ASTComponent.convertASTList(expression.getComponents()[2], 0, 0, 2);
			int choicesSize = choices.size();
			for (int c = 0; c < choicesSize; c++) {
				// process tokens individually and add them to a mapping to be settled later
				// (done so that tokens can accurately reference expressions later)
				List<Tuple<String, Boolean>> tokens = ASTComponent.convertASTList(choices.get(c), 0, 0, 2).stream().map((ASTComponent token) -> {
					ASTLiteral item = (ASTLiteral) token.getComponents()[0];
					String shortened = item.getValue().substring(1, item.getValue().length() - 1);
					if (item.getValue().startsWith("'")) {
						return new Tuple<String, Boolean>(shortened.replace("''", "'"), true);
					}
					else if (item.getValue().startsWith("<")) {
						return new Tuple<String, Boolean>(shortened, false);
					}
					else {
						throw new IllegalStateException("token: " + item.getValue() + " does not start with either \"<\" or \"'\"");
					}
				}).collect(Collectors.toList());
				
				expressionTokens.put(new Tuple<String,Integer>(expressionName, c), tokens);
			}
			// add expression with empty choices to mapping
			Expression curExpression = new Expression(new ExpressionChoice[choicesSize], expressionName);
			expressions.put(expressionName, curExpression);
			
			// set root expression if not already set
			if (rootExpression == null)
				rootExpression = curExpression;
		}
		return rootExpression;
	}

	/**
	 * reconcile expression references in the expressionTokens map to their respective expression objects
	 * @param expressions		a mapping of expression identifiers to their respective expressions
	 * @param expressionTokens	a mapping of (expression identifier, choice index) => a list of (token text, whether or not it is literal)
	 */
	private static void reconcileExpressions(Map<String, Expression> expressions,
			Map<Tuple<String, Integer>, List<Tuple<String, Boolean>>> expressionTokens) {
		
		// reconcile tokens and expressions appropriately
		for (Expression expression : expressions.values()) {
			int choiceLength = expression.getChoices().length;
			for (int c = 0; c < choiceLength; c++) {
				List<Tuple<String, Boolean>> tokenData = expressionTokens.get(new Tuple<String, Integer>(expression.getType(), c));
				if (tokenData == null)
					throw new IllegalStateException("expression type: " + expression.getType() + " does not have valid tokens for choice: " + c);
				
				Token[] arr = tokenData.stream().map((Tuple<String, Boolean> data) -> {
					if (data.getItem2()) {
						return new TokenLiteral(data.getItem1());
					}
					else {
						Expression exp = expressions.get(data.getItem1());
						if (exp == null)
							throw new IllegalStateException("token with expression reference: " + data.getItem1() + " does not exist");
						return new TokenExpression(exp);
					}
				}).toArray(Token[]::new);
				
				expression.getChoices()[c] = new ExpressionChoice(c, arr);
			}
			
			// sort array so that items with more tokens come earlier in order to prevent issues where one choice is a subset of another
			Arrays.sort(expression.getChoices(), (ExpressionChoice c1, ExpressionChoice c2) -> {
				return -Integer.compare(c1.getTokens().length, c2.getTokens().length);
			}); 
		}
	}
}
