package com.gdicristofaro.ast;

/**
 * represents a nested expression as a token 
 */
public class TokenExpression implements Token {
	private final Expression expression;

	/**
	 * main constructor
	 * @param expression		the nested expression for this token
	 */
	public TokenExpression(Expression expression) {
		this.expression = expression;
	}

	/**
	 * @return		the nested expression for this token
	 */
	public Expression getExpression() {
		return expression;
	}

	@Override
	public ParseResult handle(String s, int curIndex, RecursiveDescentParser parser) {
		// handle the expression within the recursive descent parser
		return parser.parse(s, curIndex, getExpression());
	}
}
