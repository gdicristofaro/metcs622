package com.gdicristofaro.concurrentsort.test;

import static org.junit.Assert.fail;

import java.util.Random;

import org.junit.jupiter.api.Test;

import com.gdicristofaro.concurrentsort.ConcurrentSorts;
import com.gdicristofaro.concurrentsort.ConcurrentSorts.BaseSorter;
import com.gdicristofaro.concurrentsort.Sorter;

public class SortTests {
	 

    /**
     * precondition: arr is a valid integer array
     * postcondition: array is printed to system output
     * @param arr   the array to be sorted
     */
    public static void printArr(Integer[] arr) {
        System.out.print("[");
        if (arr.length > 0) {
            System.out.print(arr[0]);

            for (int i = 1; i < arr.length; i++)
                System.out.print(", " + arr[i]);
        }
        System.out.print("]");
    }
    
    
    /**
     * performs a simple check on the array to ensure that it is sorted ascending after sorting
     * precondition: sorter/arr are valid objects
     * postcondtion: prints an error when not sorted properly
     * @param s             the sorter to check
     * @param identifier    the identifier for the sort
     * @param arr           the array to check
     */
    private static boolean checkSort(Sorter s, String identifier, Integer[] arr) {
        s.sort(arr, Integer::compare);
        boolean successful = true;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
//                System.out.println("There was an error with sort: " + identifier + " at index: " + i +
//                        " with value: " + arr[i] + " and next index: " + (i + 1) + " with value: " + arr[i+1]);
                successful = false;
            }
        }
        
        return successful;
    }


    private static int RANGE = 10000;
    
    /**
     * main method for sorting comparison that verifies the accuracy of sorts
     * please see JavaFxUi.start for chart/grid gathering implementation
     * @param args
     */
    @Test
    public void runTests() {
        Random rn = new Random();
        for (int i = 10000; i <= 100000; i+= 10000) {
            Integer[] arr = new Integer[i];
            for (int a = 0; a < i; a++)
                arr[a] = rn.nextInt(RANGE);

            if (!checkSort(BaseSorter.merge(10), "Merge Sort", arr)) fail();
            if (!checkSort(BaseSorter.mergeThreaded(10, 4, 1000), "Merge Sort (Threaded)", arr)) fail();
            if (!checkSort(BaseSorter.quick(ConcurrentSorts.MEDIAN_3, 10), "Quick Sort", arr)) fail();
            if (!checkSort(BaseSorter.quick(ConcurrentSorts.MEDIAN_9, 10), "Quick Sort (Median 9)", arr)) fail();
            if (!checkSort(BaseSorter.quickThreaded(ConcurrentSorts.MEDIAN_3, 10, 4, 1000), "Quick Sort (Threaded)", arr)) fail();
        }
    }    

    
}
