package com.gdicristofaro.concurrentsort;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * handles managing thread creation for sorting operations
 */
public class ThreadExecutor {
	private static final int MAX_MINUTES = 10;
	
	private final ThreadPoolExecutor exec;
	private final int threadMax;
	private final int minSize;
	private final boolean trackTasks;
	private final List<Future<?>> runningThreads = new ArrayList<Future<?>>();
	
	/**
	 * main constructor
	 * @param threadMax		the maximum number of threads that will be created
	 * @param minSize		the minimum size of items to be sorted where a new thread will be created
	 * @param trackTasks		tracks tasks remaining if set to true
	 */
	public ThreadExecutor(int threadMax, int minSize, boolean trackTasks) {
		this.threadMax = threadMax;
		this.minSize = minSize;
		this.trackTasks = trackTasks;
		this.exec = new ThreadPoolExecutor(threadMax, threadMax, MAX_MINUTES, TimeUnit.MINUTES,  new LinkedBlockingQueue<Runnable>());
	}

	/**
	 * executes a runnable (will create a new thread if threads available and length <= min; otherwise just run)
	 * @param len		the number of items to be sorted
	 * @param r			the runnable to be executed
	 * @return			the future for the task executing or null if run synchronously
	 */
	public Future<?> execute(int len, Runnable r) {
		if (len >= minSize) {
			synchronized(exec) {
				if (exec.getActiveCount() < threadMax) {
					Future<?> future = exec.submit(r);
					if (trackTasks)
						runningThreads.add(future);
					return future;
				}
			}
		}
		
		r.run();
		return null;
	}

	/**
	 * awaits all tasks to be run; if not tracktasks, then this will fail
	 */
	public void awaitAll() {
		if (!trackTasks)
			throw new IllegalStateException("track tasks is declared as false");
		
		try {
			int i = -1;
			while(++i < runningThreads.size())
				runningThreads.get(i).get();
			exec.shutdown();
		}
		catch (ExecutionException | InterruptedException e) {
			throw new IllegalStateException("Unable to await all due to interruption exception", e);
		}
	}
	
	/**
	 * shutsdown the operation of this executor
	 */
	public void shutdown() {
		exec.shutdown();
	}
}
