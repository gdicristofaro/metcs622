package com.gdicristofaro.concurrentsort;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ConcurrentSorts {
    /**
     * swaps values located at specific indexes in an array
     * precondition: arr is a valid array and indexA / indexB are indexes within the array
     * postcondition: those elements are swapped in the array
     * @param arr       the array
     * @param indexA    the first index
     * @param indexB    the second index
     * @param <T>       the type of the array
     */
    private static <T> void swap(T[] arr, int indexA, int indexB) {
        T temp = arr[indexA];
        arr[indexA] = arr[indexB];
        arr[indexB] = temp;
    }

    /**
     * implementation of insertion sort
     * precondition: arr is a valid array and comp is a valid comparator for arr
     * postcondition: arr is sorted min to max based off of values of comparator
     * @param arr   the array to be sorted
     * @param comp  the comparator to sort (items are sorted ascending)
     * @param first 	the first index inclusive
     * @param end	the end index exclusive
     * @param <T>   the type of the array
     */
    // end index exclusive
    public static <T> void insertionSort(T[] arr, Comparator<T> comp, int first, int end) {

        for (int i = first + 1; i < end; i++) {
            int j = i;
            while (j > first && comp.compare(arr[j], arr[j-1]) < 0) {
                swap(arr, j, j - 1);
                j--;
            }
        }
    }
    
    

    /**
     * performs the combining operation in merge sort merging sorted arrays into one sorted array
     * precondition: src, dest, comp are valid objects
     * aIndex, bIndex, end are valid indexes in the source array such that aIndex < bIndex < end
     * destIndex is a valid index in the destination array such that destIndex + (end - aIndex) - 1 is a valid index
     * postcondition: the two sorted sets of elements from the source array are combined into one sorted set in the
     * destination array at the starting location
     * @param src       the source array for the two sorted items
     * @param dest      the destination array for the sorted output
     * @param comp      the comparator to determine ascending elements
     * @param aIndex    the starting index in the source array for the first sorted set of elements
     * @param bIndex    the starting index in the source array for the second sorted set of elements
     *                  also acts as the ending index (exclusive) for the first sorted set of elements
     * @param end       the ending index (exclusive) for the second sorted set of elements
     * @param destIndex the destination start index for the combined sorted items
     * @param <T>       the array type
     */
    private static <T> void mergeCombine(T[] src, T[] dest, Comparator<T> comp,
                                         int aIndex, int bIndex, int end, int destIndex) {
        // do comparisons while there is room in each buffer
        int aEnd = bIndex;
        while (aIndex < aEnd && bIndex < end) {
            if (comp.compare(src[aIndex], src[bIndex]) <= 0)
                dest[destIndex++] = src[aIndex++];
            else
                dest[destIndex++] = src[bIndex++];
        }

        // take items from a to a and if b side is empty
        while (aIndex < aEnd)
            dest[destIndex++] = src[aIndex++];

        // take items from b to b end if a side is empty
        while (bIndex < end)
            dest[destIndex++] = src[bIndex++];
    }

    
    /**
     * the recursive helper function for merge sort
     * precondition: arr is a valid array and comp is a valid comparator for arr
     * both src and end have size to accommodate the indexes [strt, end)
     * postcondition: arr is sorted min to max based off of values of comparator from strt to end
     * @param src       the source array
     * @param dest      the destination array
     * @param comp      the comparator
     * @param strt      the starting index of items to be sorted
     * @param end       the ending index (exclusive) of items to be sorted
     * @param insertionMax	the maximum number of items that should be sorted using insertion sort
     * @param <T>       the type of the array
     */
    private static <T> void mergeRecurse(T[] src, T[] dest, Comparator<T> comp, int strt, int end, int insertionMax) {
        // base case: use insertion sort in this case
        if (end - strt <= insertionMax) {
        		insertionSort(dest, comp, strt, end);
        		return;
        }
        
        // otherwise, sort each side...
        int halfway = strt + (end - strt) / 2;
        mergeRecurse(dest, src, comp, strt, halfway, insertionMax);
        mergeRecurse(dest, src, comp, halfway, end, insertionMax);

        // then combine
        mergeCombine(src, dest, comp, strt, halfway, end, strt);
    }
    
    
    /**
     * the recursive helper function for merge sort (threaded)
     * precondition: arr is a valid array and comp is a valid comparator for arr
     * both src and end have size to accommodate the indexes [strt, end)
     * postcondition: arr is sorted min to max based off of values of comparator from strt to end
     * @param src       the source array
     * @param dest      the destination array
     * @param comp      the comparator
     * @param strt      the starting index of items to be sorted
     * @param end       the ending index (exclusive) of items to be sorted
     * @param insertionMax	the maximum number of items that should be sorted using insertion sort
     * @param exec		class to assist with thread execution
     * @param <T>       the type of the array
     */
    private static <T> void mergeRecurseThreaded(T[] src, T[] dest, Comparator<T> comp, int strt, int end, 
    												int insertionMax, ThreadExecutor exec) {
    	
        // base case: use insertion sort in this case
        if (end - strt <= insertionMax) {
        		insertionSort(dest, comp, strt, end);
        		return;
        }

        // otherwise, sort each side...
        int halfway = strt + (end - strt) / 2;
        Future<?> f = exec.execute(end - strt + 1, () -> mergeRecurseThreaded(dest, src, comp, strt, halfway, insertionMax, exec));
        mergeRecurseThreaded(dest, src, comp, halfway, end, insertionMax, exec);

        // wait on both recursions to complete
        if (f != null) {
			try {
				f.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new IllegalStateException("Unable to await merge sort thread to complete to to an error", e);
			}
        }
        
        // then combine
        mergeCombine(src, dest, comp, strt, halfway, end, strt);
    }
    

    /**
     * implementation of merge sort
     * precondition: arr is a valid array and comp is a valid comparator for arr
     * postcondition: arr is sorted min to max based off of values of comparator
     * @param arr   the array to be sorted
     * @param comp  the comparator to sort (items are sorted ascending)
     * @param insertionMax	the maximum number of items that should be sorted using insertion sort
     * @param <T>   the type of the array
     */
    public static <T> void mergeSort(T[] arr, Comparator<T> comp, int insertionMax) {
        int n = arr.length;
        @SuppressWarnings("unchecked")
		T[] scratchArr = (T[])new Object[n];
        System.arraycopy(arr, 0, scratchArr, 0, arr.length);
        mergeRecurse(scratchArr, arr, comp, 0, n, insertionMax);
    }
    
    
    /**
     * implementation of merge sort
     * precondition: arr is a valid array and comp is a valid comparator for arr
     * postcondition: arr is sorted min to max based off of values of comparator
     * @param arr   the array to be sorted
     * @param comp  the comparator to sort (items are sorted ascending)
     * @param insertionMax	the maximum number of items that should be sorted using insertion sort
     * @param threadMax		the maximum number of threads that can be executed at any given time
     * @param minSize		the minimum size of items to be sorted where a new thread will be created
     * @param <T>   the type of the array
     */
    public static <T> void mergeSortThreaded(T[] arr, Comparator<T> comp, int insertionMax, int threadMax, int minSize) {
        int n = arr.length;
        @SuppressWarnings("unchecked")
		T[] scratchArr = (T[])new Object[n];
        System.arraycopy(arr, 0, scratchArr, 0, arr.length);
        ThreadExecutor exec = new ThreadExecutor(threadMax, minSize, false);
        mergeRecurseThreaded(scratchArr, arr, comp, 0, n, insertionMax, exec);
        exec.shutdown();
    }

    
    
    /**
     * determines an index for the partition
     */
    public interface PartitionPicker {
        /**
         * determines the partition for quick sort
         * precondition: arr and comp are valid objects and startIndex/endIndex are valid indexes in the array
         * @param arr           the array to be sorted
         * @param comp          the comparator
         * @param startIndex    the starting index in the array to be sorted
         * @param endIndex      the end index in the array to be sorted
         * @param <T>           the type of the array
         * @return              the index for the partition element
         */
        public <T> int pickPartition(T[] arr, Comparator<T> comp, int startIndex, int endIndex);
    }

    /**
     * naive partition picker that picks the first element in the array
     */
    public static final PartitionPicker NAIVE = new PartitionPicker() {
        @Override
        public <T> int pickPartition(T[] arr, Comparator<T> comp, int startIndex, int endIndex) {
            return startIndex;
        }
    };

    /**
     * courtesy of the open jdk: http://grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/6-b14/java/util/Arrays.java#Arrays.med3%28int%5B%5D%2Cint%2Cint%2Cint%29
     * picks the median element index given three indexes in the array
     * precondition: x is a valid array, comp is a valid comparator, and a,b,c are valid indexes in the array
     * @param x     the array
     * @param comp  the comparator for the array
     * @param a     index a
     * @param b     index b
     * @param c     index c
     * @param <T>   the type of the array and comparator
     * @return      the index of the median element of a,b,c
     */
    private static <T> int med3(T[] x, Comparator<T> comp, int a, int b, int c) {
        return (comp.compare(x[a], x[b]) < 0 ?
                (comp.compare(x[b], x[c]) < 0 ? b : comp.compare(x[a], x[c]) < 0 ? c : a) :
                (comp.compare(x[b], x[c]) > 0 ? b : comp.compare(x[a], x[c]) > 0 ? c : a));
    }

    /**
     * partition picker that picks the median element index of beginning, middle, and end
     */
    public static final PartitionPicker MEDIAN_3 = new PartitionPicker() {
        @Override
        public <T> int pickPartition(T[] arr, Comparator<T> comp, int startIndex, int endIndex) {
            return med3(arr, comp, startIndex, endIndex - 1, (endIndex - startIndex) / 2 + startIndex);
        }
    };
    
    
    // closely modeled after: open jdk: http://grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/6-b14/java/util/Arrays.java#Arrays.med3%28int%5B%5D%2Cint%2Cint%2Cint%29
    public static final PartitionPicker MEDIAN_9 = new PartitionPicker() {
        @Override
        public <T> int pickPartition(T[] arr, Comparator<T> comp, int startIndex, int endIndex) {
        		if (endIndex - startIndex <= 40)
        			return med3(arr, comp, startIndex, endIndex - 1, (endIndex - startIndex) / 2 + startIndex);
        		
        		int s = (endIndex - startIndex) / 8;
        		int mid = (endIndex - startIndex) / 2 + startIndex;
        		
        		int first = med3(arr, comp, startIndex, startIndex + s, startIndex + 2 * s);
        		int midIndex = med3(arr, comp, mid - s, mid, mid + s);
        		int last = med3(arr, comp, endIndex, endIndex - s, endIndex - 2 * s);
        		return med3(arr, comp, first, midIndex, last);
        }   	
    };

    
    /**
     * the recursive helper for quick sort
     * precondition: arr/comp/picker are valid objects and 0 <= startIndex < endIndex <= size of array
     * postcondition: arr is sorted from [startIndex, endIndex)
     * @param arr       the array to be sorted
     * @param comp      the comparator for the array
     * @param picker    the partition picker for the array
     * @param startIndex the starting index to be sorted
     * @param endIndex  the ending index to be sorted
     * @param insertionMax	the maximum number of items that should be sorted using insertion sort
     * @param <T>       the type of array, comparator, and sorter
     */
    private static <T> void quickSortRecurse(T[] arr, Comparator<T> comp, PartitionPicker picker,
                                             int startIndex, int endIndex, int insertionMax) {

    		// base case: insertion case on smaller items 
        if (endIndex + 1 - startIndex < insertionMax) {
        		insertionSort(arr, comp, startIndex, endIndex + 1);
        		return;
        }
        
        int leftIndex = quickSortPartition(arr, comp, picker, startIndex, endIndex);

        // recurse on either side
        quickSortRecurse(arr, comp, picker, startIndex, leftIndex - 1, insertionMax);
        quickSortRecurse(arr, comp, picker, leftIndex + 1, endIndex, insertionMax);
    }
    
    
    /**
     * the recursive helper for quick sort (threaded)
     * precondition: arr/comp/picker are valid objects and 0 <= startIndex < endIndex <= size of array
     * postcondition: arr is sorted from [startIndex, endIndex)
     * @param arr       the array to be sorted
     * @param comp      the comparator for the array
     * @param picker    the partition picker for the array
     * @param startIndex the starting index to be sorted
     * @param endIndex  the ending index to be sorted
 	 * @param insertionMax	the maximum number of items that should be sorted using insertion sort
     * @param exec		class to assist with thread execution
     * @param <T>       the type of array, comparator, and sorter
     */
    private static <T> void quickSortRecurseThreaded(T[] arr, Comparator<T> comp, PartitionPicker picker,
            int startIndex, int endIndex, int insertionMax, ThreadExecutor exec) {

		// base case: insertion case on smaller items 
        if (endIndex + 1 - startIndex < insertionMax) {
        		insertionSort(arr, comp, startIndex, endIndex + 1);
        		return;
        }
        
		int leftIndex = quickSortPartition(arr, comp, picker, startIndex, endIndex);
		
		// recurse on either side
		exec.execute(leftIndex - startIndex, () -> quickSortRecurseThreaded(arr, comp, picker, startIndex, 
																		leftIndex - 1, insertionMax, exec));
		
		//quickSortRecurseThreaded(arr, comp, picker, startIndex, leftIndex - 1, insertionMax, exec);
		quickSortRecurseThreaded(arr, comp, picker, leftIndex + 1, endIndex, insertionMax, exec);
	}
    
    
    
    /**
     * partitions the array over a pivot for quick sort
     * @param arr			the array
     * @param comp			the comparator
     * @param picker			the means to pick a partition item
     * @param startIndex		the starting index for partiitioning
     * @param endIndex		the end index (inclusive) for partitioning
     * @return				the partition index after sort finished
     */
	private static <T> int quickSortPartition(T[] arr, Comparator<T> comp, PartitionPicker picker, int startIndex,
			int endIndex) {
		// get the pivot item
        int partitionIndex = picker.pickPartition(arr, comp, startIndex, endIndex);
        // put to end
        if (partitionIndex != endIndex)
            swap(arr, endIndex, partitionIndex);

        T pivot = arr[endIndex];

        // courtesy of book implementation
        int leftIndex = startIndex;
        int rightIndex = endIndex - 1;

        while (leftIndex <= rightIndex) {
            while (leftIndex <= rightIndex && comp.compare(arr[leftIndex], pivot) < 0)
                leftIndex++;
            while (leftIndex <= rightIndex && comp.compare(arr[rightIndex], pivot) >= 0)
                rightIndex--;

            if (leftIndex <= rightIndex) {
                swap(arr, leftIndex, rightIndex);
                leftIndex++;
                rightIndex--;
            }
        }

        // put the middle item where it should go
        swap(arr, endIndex, leftIndex);
		return leftIndex;
	}

    
    /**
     * implementation of quick sort
     * precondition: arr is a valid array and comp is a valid comparator for arr
     * postcondition: arr is sorted min to max based off of values of comparator
     * @param arr   the array to be sorted
     * @param comp  the comparator to sort (items are sorted ascending)
     * @param picker 	means of picking a paritition item
     * @param insertionMax 	number of elements maximum where insertion sort is used instead of quick sort 
     * @param <T>   the type of the array
     */
    public static <T> void quickSort(T[] arr, Comparator<T> comp, PartitionPicker picker, int insertionMax) {
        quickSortRecurse(arr, comp, picker, 0, arr.length - 1, insertionMax);
    }
    
    /**
     * implementation of quick sort (threaded)
     * precondition: arr is a valid array and comp is a valid comparator for arr
     * postcondition: arr is sorted min to max based off of values of comparator
     * @param arr   the array to be sorted
     * @param comp  the comparator to sort (items are sorted ascending)
     * @param picker 	means of picking a paritition item
     * @param insertionMax 	number of elements maximum where insertion sort is used instead of quick sort
     * @param threadMax		the maximum number of threads for sorting
     * @param minSize		the minimum size where new threads are still created (if thread max hasn't been reached) 
     * @param <T>   the type of the array
     */
    public static <T> void quickSortThreaded(T[] arr, Comparator<T> comp, PartitionPicker picker, int insertionMax,
    											int threadMax, int minSize) {
    	
    		ThreadExecutor exec = new ThreadExecutor(threadMax, minSize, true);
        quickSortRecurseThreaded(arr, comp, picker, 0, arr.length - 1, insertionMax, exec);
        exec.awaitAll();
    }
    
   
    /**
     * wrapper for creating quick sort or merge sort as a sorter interface item
     */
    public static class BaseSorter implements Sorter {
    		public enum SortType {MERGE, QUICK};
    		
    		private final SortType type;
    		private final boolean threaded;
		private final PartitionPicker picker;
		private final int insertionMax;
		private final int threadMax;
		private final int minSize;
    	
		
    		/**
    		 * constructor for BaseSorter
    		 * @param type			the type of sort (merge/quick)
    		 * @param threaded		whether or not this should be threaded
    		 * @param picker			for quick sort, what kind of partition picker
    		 * @param insertionMax	the maximum number of items that will be handled by insertion sort
    		 * @param threadMax		the maximum number of threads to create
    		 * @param minSize		the minimum size of items to be sorted where a new thread will be created
    		 */
		private BaseSorter(SortType type, boolean threaded, PartitionPicker picker, int insertionMax, int threadMax, int minSize) {
			this.type = type;
			this.threaded = threaded;
			this.picker = picker;
			this.insertionMax = insertionMax;
			this.threadMax = threadMax;
			this.minSize = minSize;
		}
		
		/**
		 * creates a threaded quick sort sorter
    		 * @param picker			for quick sort, what kind of partition picker
    		 * @param insertionMax	the maximum number of items that will be handled by insertion sort
    		 * @param threadMax		the maximum number of threads to create
    		 * @param minSize		the minimum size of items to be sorted where a new thread will be created
		 * @return				the generated base sorter
		 */
		public static BaseSorter quickThreaded(PartitionPicker picker, int insertionMax, int threadMax, int minSize) {
			return new BaseSorter(SortType.QUICK, true, picker, insertionMax, threadMax, minSize);
		}

		/**
		 * creates a quick sort sorter
    		 * @param picker			for quick sort, what kind of partition picker
    		 * @param insertionMax	the maximum number of items that will be handled by insertion sort
		 * @return				the generated base sorter
		 */
		public static BaseSorter quick(PartitionPicker picker, int insertionMax) {
			return new BaseSorter(SortType.QUICK, false, picker, insertionMax, 0, 0);
		}
		
		/**
		 * creates a threaded merge sort sorter
    		 * @param insertionMax	the maximum number of items that will be handled by insertion sort
    		 * @param threadMax		the maximum number of threads to create
    		 * @param minSize		the minimum size of items to be sorted where a new thread will be created
		 * @return				the generated base sorter
		 */
		public static BaseSorter mergeThreaded(int insertionMax, int threadMax, int minSize) {
			return new BaseSorter(SortType.MERGE, true, null, insertionMax, threadMax, minSize);
		}

		/**
		 * creates a merge sort sorter
    		 * @param insertionMax	the maximum number of items that will be handled by insertion sort
		 * @return				the generated base sorter
		 */
		public static BaseSorter merge(int insertionMax) {
			return new BaseSorter(SortType.MERGE, false, null, insertionMax, 0, 0);
		}

		@Override
		public <T> void sort(T[] arr, Comparator<T> c) {
			switch (type) {
			case MERGE:
				if (threaded)
					mergeSortThreaded(arr, c, insertionMax, threadMax, minSize);
				else
					mergeSort(arr, c, insertionMax);
				break;
			case QUICK:
				if (threaded)
					quickSortThreaded(arr, c, picker, insertionMax, threadMax, minSize);
				else
					quickSort(arr, c, picker, insertionMax);
				break;
			default: throw new IllegalStateException("Unknown sort type: " + type);

			}
		}
    }
}
