package com.gdicristofaro.concurrentsort;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.gdicristofaro.concurrentsort.ConcurrentSorts.BaseSorter;

import javafx.application.Application;
import javafx.beans.value.ObservableValueBase;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JavaFxUi extends Application {

	
    /**
     * defines a row of data for a sorting table
     */
    public static class TableRow {
        public final String sortType;

        // maps sort size to amount
        public final Map<Integer, Double> mapping;

        /**
         * main constructor for a table row
         * @param sortType  the sort type
         * @param mapping   mapping of array sizes to times/comparisons
         */
        public TableRow(String sortType, Map<Integer, Double> mapping) {
            this.sortType = sortType;
            this.mapping = mapping;
        }

        // creates a table row with an empty hashmap
        public TableRow(String sortType) {
            this(sortType, new HashMap<>());
        }

        public String getSortType() {
            return sortType;
        }
    }

    /**
     * an entry containing a particular sorting algorithm
     */
    private static class SortData {
        public final Sorter sort;
        public final String name;

        /**
         * main constructor specifying sort data
         * @param sort  the sorting algorithm
         * @param name  the name of the sorting algorithm
         */
        public SortData(Sorter sort, String name) {
            this.sort = sort;
            this.name = name;
        }
    }

    /**
     * sets up a chart based on data provided
     * @param rows      the rows of data forming a series in the chart
     * @param title     the title of the chart
     * @param xLabel    the label for the x axis
     * @param yLabel    the label for the y axis
     * @return          the javafx chart
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	protected LineChart<Number,Number> setupChart(Iterable<TableRow> rows, String title, String xLabel, String yLabel) {
        final NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel(xLabel);

        final NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel(yLabel);

        //creating the chart
        final LineChart<Number,Number> lineChart =
                new LineChart<>(xAxis,yAxis);

        lineChart.setTitle(title);
        //defining a series
        for (TableRow rw : rows) {
			XYChart.Series series = new XYChart.Series();
            series.setName(rw.sortType);

            for (Map.Entry<Integer, Double> ent : rw.mapping.entrySet())
                series.getData().add(new XYChart.Data(ent.getKey(), ent.getValue()));

            lineChart.getData().add(series);
        }

        return lineChart;
    }

    /**
     * set up a javafx tableview object based on the data provided
     * @param rows          the rows of data to include in the tableview
     * @param arrSizes      the array sizes for the grid's columns
     * @return              the created tableview
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	protected TableView setupGrid(List<TableRow> rows, List<Integer> arrSizes) {
        TableView table = new TableView();
        table.setEditable(true);

        double colSizeProp = 1d / (1 + arrSizes.size());
        colSizeProp = Math.floor(colSizeProp * 1000) / 1000;

        List<TableColumn> cols = new ArrayList<>();

        TableColumn<TableRow, String> sortItem = new TableColumn<>("Sort");
        sortItem.setCellValueFactory(new PropertyValueFactory<>("sortType"));
        sortItem.prefWidthProperty().bind(table.widthProperty().multiply(.99 - (arrSizes.size() * colSizeProp)));
        cols.add(sortItem);

        for (Integer size : arrSizes) {
            TableColumn<TableRow, String> sizeRow = new TableColumn<>(size.toString());
            sizeRow.setCellValueFactory(col -> new ObservableValueBase<String>() {
                @Override
                public String getValue() {
                    double val = col.getValue().mapping.get(size);
                    return Double.toString(val);
                }
            });
            sizeRow.prefWidthProperty().bind(table.widthProperty().multiply(colSizeProp * .99));
            cols.add(sizeRow);
        }

        table.getColumns().addAll(cols);

        table.setFixedCellSize(25);
        table.prefHeightProperty().set(40 + 26 * rows.size());

        table.setItems(FXCollections.observableArrayList(rows));
        return table;
    }

    /**
     * this is used to set how many runs are done.  More runs takes more time but provides more reliable results
     */
    private static int RUN_NUM = 1;

    /**
     * primary method to create the javafx ui
     * in this method, data is acquired and once acquired, presented to the user
     * @param primaryStage  the stage where the ui is to be created
     * @throws Exception    could potentially throw an exception but hopefully will not
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Sorting Comparison");

        ScrollPane sp = new ScrollPane();
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        sp.setFitToWidth(true);
        sp.setVvalue(-.5);

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(20, 20, 20, 20));
        vbox.setSpacing(30);

        sp.setContent(vbox);

        Scene scene  = new Scene(sp,1000,700);

        primaryStage.setScene(scene);
        primaryStage.show();
        showData(vbox);
    }
    
    private static final int FACTOR = 100000;
    
    private void showData(VBox vbox) {
    		List<Integer> arrSizes = new ArrayList<Integer>();
    		for (int i = 1 * FACTOR; i <= 10 * FACTOR; i += FACTOR)
    			arrSizes.add(i);
    		    
        List<SortData> sortMapping = Arrays.asList(
            new SortData(BaseSorter.merge(10), "Merge Sort"),
            new SortData(BaseSorter.mergeThreaded(10, 4, 1000), "Threaded Merge Sort"),
			new SortData(BaseSorter.quick(ConcurrentSorts.MEDIAN_9, 10), "Quick Sort"),
			new SortData(BaseSorter.quickThreaded(ConcurrentSorts.MEDIAN_9, 10, 4, 1000), "Threaded Quick Sort")
    		);
        // I wrote two different partitioning methods, the one I chose to include uses a median of three values
        // but this one just picks the first value and uses that
        //sortMapping.add(new SortData(SortingComparison.NaiveQuickSort, "naive quick"));

        List<TableRow> timeComparison = getData(sortMapping, arrSizes, RUN_NUM);
        LineChart<Number, Number> timeChart = setupChart(timeComparison, "Elapsed Time", "Array Size", "Time (in ms)");
        @SuppressWarnings("rawtypes")
		TableView timeGrid = setupGrid(timeComparison, arrSizes);

        vbox.getChildren().addAll(timeChart, timeGrid);		
	}

	/**
     * generates a random array
     * @param size      the size of the array to generate
     * @param maxNum    the maximum number such that the number is an integer [0,maxNum]
     * @return          the created random array
     */
    public static Integer[] randArr(int size, int maxNum) {
        Random r = new Random();
        Integer[] arr = new Integer[size];
        for (int i = 0; i < size; i++)
            arr[i] = r.nextInt(maxNum + 1);

        return arr;
    }


    /**
     * actually retrieves the data for each sort provided
     * @param sorts     data pertaining to the sorts to be utilized
     * @param arrSizes  the size of the arrays to use (i.e. 10000, 20000...100000)
     * @param runs      the number of runs to do in each instance.  the average value is recorded
     * @return          table data pertaining to the runs done
     */
    public static List<TableRow> getData(List<SortData> sorts, List<Integer> arrSizes, int runs) {
        HashMap<Sorter, TableRow> times = new HashMap<>();
        for (SortData s : sorts) {
            times.put(s.sort, new TableRow(s.name));
        }

        for (Integer arrSize : arrSizes) {
            for (int i = 0; i < runs; i++) {
	            Integer[] arr = randArr(arrSize, 10000000);
	            for (SortData s : sorts) {
	                Sorter sort = s.sort;
	
                    Integer[] cpy = new Integer[arr.length];
                    System.arraycopy(arr, 0, cpy, 0, arr.length);
                    System.gc();

                    long startTime = System.currentTimeMillis();
                    sort.sort(cpy, Integer::compare);
                    long endTime = System.currentTimeMillis();
                    
                    long elapsedTime = endTime - startTime;
	
                    Double prevTime = times.get(sort).mapping.get(arrSize);
                    if (prevTime == null)
                    		prevTime = 0d;
                    
	                times.get(sort).mapping.put(arrSize, prevTime + ((double)elapsedTime) / runs);
	            }
            }
        }

        List<TableRow> timesData = new ArrayList<>();

        for (SortData s : sorts) {
            timesData.add(times.get(s.sort));
        }

        return timesData;
    }

    /**
     * main method that creates the javafx ui showing charts of each run
     * on my computer, this takes a few minutes to generate and display
     * @param args          command line arguments that do nothing in this scenario
     * @throws Exception    hopefully not.
     */
    public static void main(String[] args) throws Exception {
        launch(args);
    }
}
