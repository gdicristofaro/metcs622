package com.gdicristofaro.concurrentsort;

import java.util.Comparator;

/**
 * a sorter interface to handle sorting
 */
public interface Sorter {
	/**
	 * sorts the array arr
	 * @param arr		the array to sort
	 * @param c			the comparator to use
	 */
	public <T> void sort(T[] arr, Comparator<T> c);
}